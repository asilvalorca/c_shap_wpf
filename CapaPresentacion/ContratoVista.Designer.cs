﻿namespace CapaPresentacion
{
    partial class ContratoVista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContratoVista));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtContrato = new System.Windows.Forms.TextBox();
            this.txtRutCliente = new System.Windows.Forms.TextBox();
            this.comboFechaFinal = new System.Windows.Forms.DateTimePicker();
            this.comboFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.txtPrimaAnual = new System.Windows.Forms.TextBox();
            this.txtPrimaMensual = new System.Windows.Forms.TextBox();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.btnCancelarContrato = new System.Windows.Forms.Button();
            this.comboPlan = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDigito = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtApellidoCliente = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.dateTimeFechaNacimiento = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPoliza = new System.Windows.Forms.TextBox();
            this.checkVigencia = new System.Windows.Forms.CheckBox();
            this.btnBuscarCliente = new System.Windows.Forms.Button();
            this.btnNuevoContrato = new System.Windows.Forms.Button();
            this.btnEliminarContrato = new System.Windows.Forms.Button();
            this.btnBuscarContrato = new System.Windows.Forms.Button();
            this.checkDeclaracion = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnEditarContrato = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // txtContrato
            // 
            resources.ApplyResources(this.txtContrato, "txtContrato");
            this.txtContrato.Name = "txtContrato";
            // 
            // txtRutCliente
            // 
            resources.ApplyResources(this.txtRutCliente, "txtRutCliente");
            this.txtRutCliente.Name = "txtRutCliente";
            // 
            // comboFechaFinal
            // 
            resources.ApplyResources(this.comboFechaFinal, "comboFechaFinal");
            this.comboFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.comboFechaFinal.Name = "comboFechaFinal";
            // 
            // comboFechaInicio
            // 
            this.comboFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.comboFechaInicio, "comboFechaInicio");
            this.comboFechaInicio.Name = "comboFechaInicio";
            // 
            // txtPrimaAnual
            // 
            resources.ApplyResources(this.txtPrimaAnual, "txtPrimaAnual");
            this.txtPrimaAnual.Name = "txtPrimaAnual";
            // 
            // txtPrimaMensual
            // 
            resources.ApplyResources(this.txtPrimaMensual, "txtPrimaMensual");
            this.txtPrimaMensual.Name = "txtPrimaMensual";
            // 
            // txtObs
            // 
            resources.ApplyResources(this.txtObs, "txtObs");
            this.txtObs.Name = "txtObs";
            // 
            // btnCancelarContrato
            // 
            resources.ApplyResources(this.btnCancelarContrato, "btnCancelarContrato");
            this.btnCancelarContrato.Name = "btnCancelarContrato";
            this.btnCancelarContrato.UseVisualStyleBackColor = true;
            this.btnCancelarContrato.Click += new System.EventHandler(this.btnCancelarContrato_Click);
            // 
            // comboPlan
            // 
            resources.ApplyResources(this.comboPlan, "comboPlan");
            this.comboPlan.FormattingEnabled = true;
            this.comboPlan.Name = "comboPlan";
            this.comboPlan.SelectedIndexChanged += new System.EventHandler(this.comboPlan_SelectedIndexChanged);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // txtDigito
            // 
            resources.ApplyResources(this.txtDigito, "txtDigito");
            this.txtDigito.Name = "txtDigito";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // txtApellidoCliente
            // 
            resources.ApplyResources(this.txtApellidoCliente, "txtApellidoCliente");
            this.txtApellidoCliente.Name = "txtApellidoCliente";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // txtNombre
            // 
            resources.ApplyResources(this.txtNombre, "txtNombre");
            this.txtNombre.Name = "txtNombre";
            // 
            // dateTimeFechaNacimiento
            // 
            resources.ApplyResources(this.dateTimeFechaNacimiento, "dateTimeFechaNacimiento");
            this.dateTimeFechaNacimiento.Name = "dateTimeFechaNacimiento";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // txtPoliza
            // 
            resources.ApplyResources(this.txtPoliza, "txtPoliza");
            this.txtPoliza.Name = "txtPoliza";
            // 
            // checkVigencia
            // 
            resources.ApplyResources(this.checkVigencia, "checkVigencia");
            this.checkVigencia.Checked = true;
            this.checkVigencia.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkVigencia.Name = "checkVigencia";
            this.checkVigencia.UseVisualStyleBackColor = true;
            // 
            // btnBuscarCliente
            // 
            resources.ApplyResources(this.btnBuscarCliente, "btnBuscarCliente");
            this.btnBuscarCliente.Name = "btnBuscarCliente";
            this.btnBuscarCliente.UseVisualStyleBackColor = true;
            this.btnBuscarCliente.Click += new System.EventHandler(this.btnBuscarCliente_Click);
            // 
            // btnNuevoContrato
            // 
            resources.ApplyResources(this.btnNuevoContrato, "btnNuevoContrato");
            this.btnNuevoContrato.Name = "btnNuevoContrato";
            this.btnNuevoContrato.UseVisualStyleBackColor = true;
            this.btnNuevoContrato.Click += new System.EventHandler(this.btnNuevoContrato_Click);
            // 
            // btnEliminarContrato
            // 
            resources.ApplyResources(this.btnEliminarContrato, "btnEliminarContrato");
            this.btnEliminarContrato.Name = "btnEliminarContrato";
            this.btnEliminarContrato.UseVisualStyleBackColor = true;
            this.btnEliminarContrato.Click += new System.EventHandler(this.btnEliminarContrato_Click);
            // 
            // btnBuscarContrato
            // 
            resources.ApplyResources(this.btnBuscarContrato, "btnBuscarContrato");
            this.btnBuscarContrato.Name = "btnBuscarContrato";
            this.btnBuscarContrato.UseVisualStyleBackColor = true;
            this.btnBuscarContrato.Click += new System.EventHandler(this.btnBuscarContrato_Click);
            // 
            // checkDeclaracion
            // 
            resources.ApplyResources(this.checkDeclaracion, "checkDeclaracion");
            this.checkDeclaracion.Name = "checkDeclaracion";
            this.checkDeclaracion.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // btnEditarContrato
            // 
            resources.ApplyResources(this.btnEditarContrato, "btnEditarContrato");
            this.btnEditarContrato.Name = "btnEditarContrato";
            this.btnEditarContrato.UseVisualStyleBackColor = true;
            this.btnEditarContrato.Click += new System.EventHandler(this.btnEditarContrato_Click);
            // 
            // btnLimpiar
            // 
            resources.ApplyResources(this.btnLimpiar, "btnLimpiar");
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // ContratoVista
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.checkDeclaracion);
            this.Controls.Add(this.checkVigencia);
            this.Controls.Add(this.txtPoliza);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.dateTimeFechaNacimiento);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtApellidoCliente);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnBuscarCliente);
            this.Controls.Add(this.txtDigito);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboPlan);
            this.Controls.Add(this.btnCancelarContrato);
            this.Controls.Add(this.btnBuscarContrato);
            this.Controls.Add(this.btnEditarContrato);
            this.Controls.Add(this.btnEliminarContrato);
            this.Controls.Add(this.btnNuevoContrato);
            this.Controls.Add(this.txtObs);
            this.Controls.Add(this.txtPrimaAnual);
            this.Controls.Add(this.txtPrimaMensual);
            this.Controls.Add(this.comboFechaInicio);
            this.Controls.Add(this.comboFechaFinal);
            this.Controls.Add(this.txtRutCliente);
            this.Controls.Add(this.txtContrato);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ContratoVista";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtContrato;
        private System.Windows.Forms.TextBox txtRutCliente;
        private System.Windows.Forms.DateTimePicker comboFechaFinal;
        private System.Windows.Forms.DateTimePicker comboFechaInicio;
        private System.Windows.Forms.TextBox txtPrimaAnual;
        private System.Windows.Forms.TextBox txtPrimaMensual;
        private System.Windows.Forms.TextBox txtObs;
        private System.Windows.Forms.Button btnNuevoContrato;
        private System.Windows.Forms.Button btnEliminarContrato;
        private System.Windows.Forms.Button btnBuscarContrato;
        private System.Windows.Forms.Button btnCancelarContrato;
        private System.Windows.Forms.ComboBox comboPlan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDigito;
        private System.Windows.Forms.Button btnBuscarCliente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtApellidoCliente;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox dateTimeFechaNacimiento;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPoliza;
        private System.Windows.Forms.CheckBox checkVigencia;
        private System.Windows.Forms.CheckBox checkDeclaracion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnEditarContrato;
        private System.Windows.Forms.Button btnLimpiar;
    }
}