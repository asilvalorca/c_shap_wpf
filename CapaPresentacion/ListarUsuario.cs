﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class ListarUsuario : Form
    {
       
        public ListarUsuario()
        {
            InitializeComponent();

           
            Sexo sexo = new Sexo();

            this.comboSexo.Items.Add("Debe Seleccionar Sexo");
            List<Sexo> listSexo = sexo.ListarSexos();
            foreach (Sexo obj in listSexo)
            {

                this.comboSexo.Items.Add(obj.Descripcion);
            }
            EstadoCivil estadoCivil = new EstadoCivil();
            List<EstadoCivil> listEstadoCivil = estadoCivil.ListarEstadoCiviles();
            this.comboEstadoCivil.Items.Add("Debe Seleccionar Estado Civil");
            foreach (EstadoCivil obj in listEstadoCivil)
            {

                this.comboEstadoCivil.Items.Add(obj.Descripcion);
            }
        }
        public void CargaTitulo()
        {
            DataGridViewTextBoxColumn c1 = new DataGridViewTextBoxColumn();
            c1.HeaderText = "Rut";
            c1.ReadOnly = true;
            dataListarUsuario.Columns.Add(c1);
            c1 = null;

            DataGridViewTextBoxColumn c2 = new DataGridViewTextBoxColumn();
            c2.HeaderText = "Nombre";
            c2.ReadOnly = true;
            dataListarUsuario.Columns.Add(c2);
            c2 = null;

            DataGridViewTextBoxColumn c3 = new DataGridViewTextBoxColumn();
            c3.HeaderText = "Apellido";
            c3.ReadOnly = true;
            dataListarUsuario.Columns.Add(c3);
            c3 = null;

            DataGridViewTextBoxColumn c4 = new DataGridViewTextBoxColumn();
            c4.HeaderText = "FechaNacimiento";
            c4.ReadOnly = true;
            dataListarUsuario.Columns.Add(c4);
            c4 = null;

            DataGridViewTextBoxColumn c5 = new DataGridViewTextBoxColumn();
            c5.HeaderText = "Sexo";
            c5.ReadOnly = true;
            dataListarUsuario.Columns.Add(c5);
            c5 = null;

            DataGridViewTextBoxColumn c6 = new DataGridViewTextBoxColumn();
            c6.HeaderText = "EstadoCivil";
            c6.ReadOnly = true;
            dataListarUsuario.Columns.Add(c6);
            c6 = null;
        }


        private void ListarUsuario_Load(object sender, EventArgs e)
        {

            CargaTitulo();
          

        }
        public void LoadAllUsers()
        {
            
            Cliente cliente = new Cliente();
            List<Cliente> listCliente = new List<Cliente>();
            listCliente = cliente.ListarClienteDetalle();
            if (listCliente.Count()==0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }
             

            foreach (Cliente c in listCliente)
            {
                //VentanaMensaje.ventana(Convert.ToDateTime(c.FechaNacimiento).ToString("dd/MM/yyyy"));
                dataListarUsuario.Rows.Add(c.RutCliente, c.Nombres, c.Apellidos, Convert.ToDateTime(c.FechaNacimiento).ToString("dd/MM/yyyy"), c.Sexo, c.EstadoCivil);
            }

        }
        public void LoadUser(string rut, int idsexo, int idestadocivil)
        {

            Cliente cliente = new Cliente();
            List<Cliente> listCliente = new List<Cliente>();
            listCliente =  cliente.ListarClienteDetalleOpcion(rut, idsexo, idestadocivil);

            if (listCliente.Count() == 0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }
            foreach (Cliente c in listCliente)
            {
                //VentanaMensaje.ventana("entro __");
                dataListarUsuario.Rows.Add(c.RutCliente, c.Nombres, c.Apellidos, Convert.ToDateTime(c.FechaNacimiento).ToString("dd/MM/yyyy"), c.Sexo, c.EstadoCivil);
            }

        }


        private void DataListarUsuario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cliente c = new Cliente();
            DataGridViewRow row = dataListarUsuario.Rows[e.RowIndex];
            //VentanaMensaje.ventana("sin conten"+dataListarUsuario.CurrentRow.Cells[0].Value.ToString());
            try
            {

                c.RutCliente = dataListarUsuario.CurrentRow.Cells[0].Value.ToString();
                List<Cliente> listUsuario = new List<Cliente>();
                listUsuario = c.ListarClienteId(c);
                foreach (Cliente cli in listUsuario)
                {
                    c.RutCliente = cli.RutCliente;
                    c.Nombres = cli.Nombres;
                    c.Apellidos = cli.Apellidos;
                    c.FechaNacimiento = cli.FechaNacimiento;
                    c.IdEstadoCivil = cli.IdEstadoCivil;
                    c.IdSexo = cli.IdSexo;
                }

                IFormCliente miInterfaz = this.Owner as IFormCliente;
                if (miInterfaz != null)
                    miInterfaz.CargarClienteEncontrado(c);
                this.Dispose();
            }
            catch
            {

            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dataListarUsuario.Rows.Clear();
            //CargaTitulo();
            if ((this.comboSexo.SelectedIndex==-1 || this.comboSexo.SelectedIndex == 0) && (this.comboEstadoCivil.SelectedIndex == -1 || this.comboEstadoCivil.SelectedIndex == 0) && this.txtDigito.Text.Trim()=="" && this.txtRut.Text.Trim() == "")
            {
                LoadAllUsers();
            }else
            {
                LoadUser(this.txtRut.Text.Trim()+"-"+this.txtDigito.Text.Trim(), this.comboSexo.SelectedIndex, this.comboEstadoCivil.SelectedIndex);
            }

           
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            this.txtRut.Text = "";
            this.txtDigito.Text = "";
            this.comboEstadoCivil.SelectedIndex = -1;
            this.comboSexo.SelectedIndex = -1;
        }

        private void txtRut_TextChanged(object sender, EventArgs e)
        {
            txtRut.Text = Validar.ValidaNumero(txtRut.Text);
            txtRut.Select(txtRut.Text.Length, 0);
        }
    }
}
