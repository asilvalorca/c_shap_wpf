﻿namespace CapaPresentacion
{
    partial class ListaContrato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridDatos = new System.Windows.Forms.DataGridView();
            this.textContrato = new System.Windows.Forms.TextBox();
            this.txtRut = new System.Windows.Forms.TextBox();
            this.txtDigito = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboPoliza = new System.Windows.Forms.ComboBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Poliza = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridDatos
            // 
            this.dataGridDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDatos.Location = new System.Drawing.Point(12, 118);
            this.dataGridDatos.Name = "dataGridDatos";
            this.dataGridDatos.Size = new System.Drawing.Size(771, 150);
            this.dataGridDatos.TabIndex = 0;
            this.dataGridDatos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataListarContrato_CellDoubleClick);
            // 
            // textContrato
            // 
            this.textContrato.Location = new System.Drawing.Point(13, 45);
            this.textContrato.Name = "textContrato";
            this.textContrato.Size = new System.Drawing.Size(114, 20);
            this.textContrato.TabIndex = 1;
            this.textContrato.TextChanged += new System.EventHandler(this.txtContrato_TextChanged);
            // 
            // txtRut
            // 
            this.txtRut.Location = new System.Drawing.Point(196, 45);
            this.txtRut.Name = "txtRut";
            this.txtRut.Size = new System.Drawing.Size(100, 20);
            this.txtRut.TabIndex = 2;
            this.txtRut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRut.TextChanged += new System.EventHandler(this.txtRut_TextChanged);
            // 
            // txtDigito
            // 
            this.txtDigito.Location = new System.Drawing.Point(318, 45);
            this.txtDigito.Name = "txtDigito";
            this.txtDigito.Size = new System.Drawing.Size(25, 20);
            this.txtDigito.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Contrato";
            // 
            // comboPoliza
            // 
            this.comboPoliza.FormattingEnabled = true;
            this.comboPoliza.Location = new System.Drawing.Point(410, 47);
            this.comboPoliza.Name = "comboPoliza";
            this.comboPoliza.Size = new System.Drawing.Size(121, 21);
            this.comboPoliza.TabIndex = 5;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(589, 45);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(683, 45);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 7;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Rut Cliente";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(302, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "-";
            // 
            // Poliza
            // 
            this.Poliza.AutoSize = true;
            this.Poliza.Location = new System.Drawing.Point(407, 23);
            this.Poliza.Name = "Poliza";
            this.Poliza.Size = new System.Drawing.Size(35, 13);
            this.Poliza.TabIndex = 10;
            this.Poliza.Text = "Poliza";
            // 
            // ListaContrato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 280);
            this.Controls.Add(this.Poliza);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.comboPoliza);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDigito);
            this.Controls.Add(this.txtRut);
            this.Controls.Add(this.textContrato);
            this.Controls.Add(this.dataGridDatos);
            this.Name = "ListaContrato";
            this.Text = "ListaContrato";
            this.Load += new System.EventHandler(this.ListaContrato_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDatos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridDatos;
        private System.Windows.Forms.TextBox textContrato;
        private System.Windows.Forms.TextBox txtRut;
        private System.Windows.Forms.TextBox txtDigito;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboPoliza;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Poliza;
    }
}