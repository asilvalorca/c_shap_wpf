﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class ListaContrato : Form
    {
        public ListaContrato()
        {
            InitializeComponent();
            Plan plan = new Plan();
            CargaTitulo();
            this.comboPoliza.Items.Add("Poliza");
            List<Plan> listPlan = plan.listarPlanes();
            foreach (Plan obj in listPlan)
            {

                this.comboPoliza.Items.Add(obj.PolizaActual);
            }
        }
        public void Limpiar()
        {

        }
        public void CargaTitulo()
        {
            DataGridViewTextBoxColumn c1 = new DataGridViewTextBoxColumn();
            c1.HeaderText = "Contrato";
            c1.ReadOnly = true;
            dataGridDatos.Columns.Add(c1);
            c1 = null;

            DataGridViewTextBoxColumn c2 = new DataGridViewTextBoxColumn();
            c2.HeaderText = "Rut";
            c2.ReadOnly = true;
            dataGridDatos.Columns.Add(c2);
            c2 = null;

            DataGridViewTextBoxColumn c3 = new DataGridViewTextBoxColumn();
            c3.HeaderText = "Nombre";
            c3.ReadOnly = true;
            dataGridDatos.Columns.Add(c3);
            c3 = null;

            DataGridViewTextBoxColumn c4 = new DataGridViewTextBoxColumn();
            c4.HeaderText = "Apellido";
            c4.ReadOnly = true;
            dataGridDatos.Columns.Add(c4);
            c4 = null;

            DataGridViewTextBoxColumn c5 = new DataGridViewTextBoxColumn();
            c5.HeaderText = "Nombre Plan";
            c5.ReadOnly = true;
            dataGridDatos.Columns.Add(c5);
            c5 = null;

            DataGridViewTextBoxColumn c6 = new DataGridViewTextBoxColumn();
            c6.HeaderText = "Poliza";
            c6.ReadOnly = true;
            dataGridDatos.Columns.Add(c6);
            c6 = null;

           
        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            this.textContrato.Text = "";
            this.txtRut.Text = "";
            this.txtDigito.Text = "";
            this.comboPoliza.SelectedIndex = -1;
        }

        private void ListaContrato_Load(object sender, EventArgs e)
        {
            
        }
        public void LoadAllContrato()
        {

            Contrato contrato = new Contrato();
            List<Contrato> listContrato = new List<Contrato>();
            listContrato = contrato.ListarContratoDetalle();
            if (listContrato.Count() == 0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }


            foreach (Contrato c in listContrato)
            {
                //VentanaMensaje.ventana(Convert.ToDateTime(c.FechaNacimiento).ToString("dd/MM/yyyy"));
                dataGridDatos.Rows.Add(c.Numero, c.RutCliente, c.Nombre, c.Apellido, c.Plan, c.Poliza);
            }

        }
        public void LoadUser(string rut, string numero, string poliza)
        {

            Contrato contrato = new Contrato();
            List<Contrato> listContrato = new List<Contrato>();
            listContrato = contrato.ListarContratoDetalleOpcion(rut, numero, poliza);

            if (listContrato.Count() == 0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }
            foreach (Contrato c in listContrato)
            {
                //VentanaMensaje.ventana("entro __");
                dataGridDatos.Rows.Add(c.Numero, c.RutCliente, c.Nombre, c.Apellido, c.Plan, c.Poliza);
            }

        }


        private void DataListarContrato_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

          
            Contrato c = new Contrato();
            DataGridViewRow row = dataGridDatos.Rows[e.RowIndex];
            //VentanaMensaje.ventana("sin conten"+dataListarUsuario.CurrentRow.Cells[0].Value.ToString());
            try
            {
                
                c.Numero = dataGridDatos.CurrentRow.Cells[0].Value.ToString();
               // VentanaMensaje.ventana(c.Numero);
                List<Contrato> listContrato = new List<Contrato>();
                listContrato = c.ListarContratoId(c);
                //VentanaMensaje.ventana("NUMERO"+ listContrato.Count());
                foreach (Contrato ct in listContrato)
                {
                    //VentanaMensaje.ventana(ct.FechaNacimiento.ToString());
                    c.RutCliente = ct.RutCliente;
                    c.Nombre = ct.Nombre;
                    c.Apellido = ct.Apellido;
                    c.Numero = ct.Numero;
                    c.FechaNacimiento = ct.FechaNacimiento;
                    c.FechaInicioVigencia = ct.FechaInicioVigencia;
                    c.Poliza = ct.Poliza;
                    c.PrimaAnual = ct.PrimaAnual;
                    c.PrimaMensual = ct.PrimaMensual;
                    c.Vigencia = ct.Vigencia;
                    c.Observaciones = ct.Observaciones;
                    c.CodigoPlan = ct.CodigoPlan;
                    c.DeclaracionSalud = ct.DeclaracionSalud;
                    //c.IdEstadoCivil = ct.IdEstadoCivil;
                    // c.IdSexo = ct.IdSexo;

                }

                IFormCliente miInterfaz2 = this.Owner as IFormCliente;
                if (miInterfaz2 != null)
                    miInterfaz2.CargarContratoEncontrado(c);
                this.Dispose();
            }
            catch
            {

            }

        
    }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //VentanaMensaje.ventana(txtRut.Text.Trim() + "-" + this.txtDigito.Text.Trim()+"__" + this.textContrato.Text.Trim()+"__"+ this.comboPoliza.SelectedItem);
            dataGridDatos.Rows.Clear();
            //CargaTitulo();
            if ((this.comboPoliza.SelectedIndex == -1 || this.comboPoliza.SelectedIndex == 0) && this.txtDigito.Text.Trim() == "" && this.txtRut.Text.Trim() == "" && textContrato.Text.Trim()=="")
            {
                LoadAllContrato();
            }
            else
            {
                string rut = "";
                if (this.txtRut.Text.Trim()!="" && this.txtDigito.Text.Trim()!="")
                {
                    rut = this.txtRut.Text.Trim() + "-" + this.txtDigito.Text.Trim();
                }
                LoadUser(rut, this.textContrato.Text.Trim(), this.comboPoliza.SelectedItem.ToString());
            }

        }

        private void txtContrato_TextChanged(object sender, EventArgs e)
        {
            textContrato.Text = Validar.ValidaNumero(textContrato.Text);
            textContrato.Select(textContrato.Text.Length, 0);
            
        }

        private void txtRut_TextChanged(object sender, EventArgs e)
        {
            txtRut.Text = Validar.ValidaNumero(txtRut.Text);
            txtRut.Select(txtRut.Text.Length, 0);
        }
    }
}
