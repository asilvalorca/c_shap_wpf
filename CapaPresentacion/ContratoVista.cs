﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using System.Text.RegularExpressions;

namespace CapaPresentacion
{
    public partial class ContratoVista : Form, IFormCliente
    {
        public ContratoVista()
        {
            InitializeComponent();
            Plan plan = new Plan();

            this.comboPlan.Items.Add("Debe Seleccionar Plan");
            List<Plan> listPlan = plan.listarPlanes();
            foreach (Plan obj in listPlan)
            {

                this.comboPlan.Items.Add(obj.IdPlan);
            }
        }
        public void CargarClienteEncontrado(Cliente cli)
        {
            //VentanaMensaje.ventana("paso");
           
            string input = cli.RutCliente;
            string pattern = "-";            // Split on hyphens
            comboPlan.Enabled = true;
            string[] substrings = Regex.Split(input, pattern);           
            this.txtRutCliente.Text = substrings[0];
            this.txtDigito.Text = substrings[1];
            this.txtNombre.Text = cli.Nombres;
            this.txtApellidoCliente.Text = cli.Apellidos;           
            this.dateTimeFechaNacimiento.Text = Convert.ToDateTime(cli.FechaNacimiento).ToString("dd/MM/yyyy");
            this.checkVigencia.Checked = true;


        }

        public void CargarContratoEncontrado(Contrato contrato)
        {
           // VentanaMensaje.ventana(contrato.CodigoPlan);

            string input = contrato.RutCliente;
            string pattern = "-";
            // Split on hyphens
            /*          btnNuevoContrato
                        string input2 = contrato.CodigoPlan;
                        string pattern2 = "VID0";            
                        */
            btnEditarContrato.Enabled = true;
            btnEliminarContrato.Enabled = true; 
            btnNuevoContrato.Enabled = false;
            btnBuscarCliente.Enabled = false;
            string[] substrings = Regex.Split(input, pattern);
            this.txtRutCliente.Text = substrings[0];
            this.txtDigito.Text = substrings[1];
            this.txtNombre.Text = contrato.Nombre;
            this.txtApellidoCliente.Text = contrato.Apellido;
            this.txtObs.Text = contrato.Observaciones;
            this.txtContrato.Text = contrato.Numero;
            this.comboFechaInicio.Value = contrato.FechaInicioVigencia;
            this.dateTimeFechaNacimiento.Text = Convert.ToDateTime(contrato.FechaNacimiento).ToString("dd/MM/yyyy");
            this.txtPrimaAnual.Text = contrato.PrimaAnual.ToString();
            this.txtPrimaMensual.Text = contrato.PrimaMensual.ToString();
            this.txtPoliza.Text = contrato.Poliza;           
            this.comboPlan.SelectedItem = contrato.CodigoPlan;
            this.checkVigencia.Checked = false;
            this.comboPlan.Enabled = false;

            if (Convert.ToInt32(contrato.Vigencia) == 1)
            {
                this.checkVigencia.Checked = true;
            }
            this.checkDeclaracion.Checked = false;
            if (Convert.ToInt32(contrato.DeclaracionSalud) == 1)
            {
                this.checkDeclaracion.Checked = true;
            }

            //this.dateTimeFechaNacimiento.Text = Convert.ToDateTime(contrato.FechaNacimiento).ToString("dd/MM/yyyy");


        }
        public void Limpiar()
        {
            this.txtContrato.Text = "";
            this.txtRutCliente.Text = "";
            this.txtDigito.Text = "";
            this.txtNombre.Text = "";
            this.txtApellidoCliente.Text = "";
            this.dateTimeFechaNacimiento.Text = "";
            this.comboPlan.SelectedIndex = -1;
            this.txtPoliza.Text = "";
            btnNuevoContrato.Enabled = true;
            this.txtPrimaAnual.Text = "";
            this.txtPrimaMensual.Text = "";
            this.txtObs.Text = "";
            btnBuscarCliente.Enabled = true; 
            this.checkDeclaracion.Checked = false;
            this.comboPlan.Enabled = false;
            btnEditarContrato.Enabled = false;
            btnEliminarContrato.Enabled = false;
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelarContrato_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 m = new Form1();
            m.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 m = new Form1();
            m.Show();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            ListarUsuario lu = new ListarUsuario();
            lu.Show(this);
            lu.LoadAllUsers();
        }

        private void btnNuevoContrato_Click(object sender, EventArgs e)
        {
            Validar valida = new Validar();
            string day = DateTime.Now.ToString("dd");
            string mes = DateTime.Now.ToString("MM");
            string year = DateTime.Now.ToString("yyyy");
            string hora = DateTime.Now.ToString("hh");
            string minuto = DateTime.Now.ToString("mm");
            string segundo = DateTime.Now.ToString("ss");
            DateTime thisDay = DateTime.Today;

            string a = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(comboFechaInicio.Value.ToString()) );

            string m = valida.DiferenciaFechas_mes(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(comboFechaInicio.Value.ToString()));

            string d = valida.DiferenciaFechas_dia(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(comboFechaInicio.Value.ToString()));

            if (a.Equals("Fecha Invalida")|| m.Equals("Fecha Invalida"))
            {
                
                string a1 = valida.DiferenciaFechas_anio(DateTime.Parse(comboFechaInicio.Value.ToString()), DateTime.Parse(thisDay.ToString("d")));

                string a2 = valida.DiferenciaFechas_mes(DateTime.Parse(comboFechaInicio.Value.ToString()), DateTime.Parse(thisDay.ToString("d")));

                string a3 = valida.DiferenciaFechas_dia(DateTime.Parse(comboFechaInicio.Value.ToString()), DateTime.Parse(thisDay.ToString("d")));

                if (Convert.ToInt32(a1) > 0)
                {
                    VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                    return;
                }else
                {
                    if (Convert.ToInt32(a2) > 0)
                    {
                        if (Convert.ToInt32(a2) == 1)
                        {
                            if (Convert.ToInt32(a3) == 0)
                            {

                            }
                            else
                            {
                                VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                            }
                        }
                        else
                        {
                            VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                        }
                    }else
                    {

                    }
                }
            }
            else
            {
               
                if (Convert.ToInt32(a)==0 && Convert.ToInt32(m) == 0 && Convert.ToInt32(d) == 0)
                {

                }
                else{
                    VentanaMensaje.ventana("La fecha no puede ser menor a la actual");
                    return;
                }
            }
            
           



           
            if (this.txtRutCliente.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Seleccionar un Cliente");
                return;
            }

            if (this.comboPlan.SelectedIndex== -1 || this.comboPlan.SelectedIndex == 0)
            {
                VentanaMensaje.ventana("Debe Seleccionar un Plan");
                return;
            }
           

            List<Cliente> listcli = new List<Cliente>();
            Contrato contrato = new Contrato();
            contrato.Numero = year+mes+day+hora+minuto+segundo;
            contrato.RutCliente = this.txtRutCliente.Text+"-"+ this.txtDigito.Text;
            contrato.FechaCreacion = DateTime.Today;
            contrato.CodigoPlan = "VID0" + this.comboPlan.SelectedIndex;
            contrato.FechaInicioVigencia = this.comboFechaInicio.Value;
            contrato.FechaFinVigencia = this.comboFechaFinal.Value;
            contrato.Vigencia = Convert.ToBoolean(this.checkVigencia.Checked?1:0);
            contrato.DeclaracionSalud = Convert.ToBoolean(this.checkDeclaracion.Checked ? 1 : 0); 
            contrato.PrimaAnual = Convert.ToSingle( this.txtPrimaAnual.Text);
            contrato.PrimaMensual = Convert.ToSingle(this.txtPrimaMensual.Text);
            contrato.Observaciones = txtObs.Text.Trim();

            Cliente cliente = new Cliente();
            cliente.RutCliente = txtRutCliente + "-" + txtDigito;
            listcli = contrato.BuscarCliente(cliente);
          
                    if (contrato.Agregar(contrato))
                    {
                        VentanaMensaje.ventana("Contrato ingresado correctamente ");
                        this.Limpiar();
                    }
                    else
                    {
                        VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momento !! c", "Advertencia!");
                    }
        
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
       

      

        private void comboPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime thisDay = DateTime.Today;

            if (txtRutCliente.Text == "")
            {
                //VentanaMensaje.ventana("Primero debe seleccionar un Cliente");
                this.comboPlan.SelectedIndex = -1;
                return;
            }
            if (this.comboPlan.SelectedIndex != -1 || this.comboPlan.SelectedIndex != 0)
            {
                Plan p = new Plan();
                Validar valida = new Validar();
                List<Plan> listPlan = new List<Plan>();
                p.IdPlan = "VID0" + this.comboPlan.SelectedIndex;
                listPlan = p.ListarPlanId(p);
                if (listPlan != null)
                {
                    Cliente cliente = new Cliente();
                    List<Cliente> listCliente = new List<Cliente>();
                    cliente.RutCliente = txtRutCliente + "-" + txtDigito;
                    cliente.ListarClienteId(cliente);
                    foreach (Cliente c in listCliente)
                    {
                        cliente.RutCliente = c.RutCliente;
                        cliente.Nombres = c.Nombres;
                        cliente.Apellidos = c.Apellidos;
                        cliente.FechaNacimiento = c.FechaNacimiento;
                        cliente.IdSexo = c.IdSexo;
                        cliente.IdEstadoCivil = c.IdEstadoCivil;
                    }
                    foreach (Plan plan in listPlan)
                    {
                        string str = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(cliente.FechaNacimiento.ToString()));
                        int edad = Convert.ToInt32(str);
                        double recargo = 0;
                        if (edad >= 18 && edad <= 25)
                        {
                            recargo = recargo + 3.6;
                        } else if (edad >= 26 && edad <= 45)
                        {
                            recargo = recargo + 2.4;
                        }
                        else if (edad > 45)
                        {
                            recargo = recargo + 6.0;
                        }

                        if (cliente.IdSexo == 1)
                        {
                            recargo = recargo + 2.4;

                        } else if(cliente.IdSexo == 2){
                            recargo = recargo + 1.2;

                        }

                        if (cliente.IdEstadoCivil == 1)
                        {
                            recargo = recargo + 4.8;
                        }
                        else if (cliente.IdEstadoCivil == 2)
                        {
                            recargo = recargo + 2.4;
                        }
                        else if (cliente.IdEstadoCivil > 2)
                        {
                            recargo = recargo + 3.6;
                        }
                        txtPoliza.Text = plan.PolizaActual;
                        txtPrimaAnual.Text = (plan.PrimaBase + recargo).ToString();
                        txtPrimaMensual.Text = ((plan.PrimaBase + recargo)/12).ToString();
                    }
                }
            }
            else{
                txtPoliza.Text = "";
            }
        }

        private void btnBuscarContrato_Click(object sender, EventArgs e)
        {
            ListaContrato lc = new ListaContrato();
            
            
            lc.LoadAllContrato();
            lc.Show(this);
        }

        private void comboDeclaracion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnEliminarContrato_Click(object sender, EventArgs e)
        {
            DateTime thisDay = DateTime.Today;
            Contrato contrato = new Contrato();
            contrato.Vigencia = Convert.ToBoolean(1);
            contrato.Numero = txtContrato.Text;
            contrato.FechaFinVigencia = DateTime.Parse(thisDay.ToString("d"));
            if (checkVigencia.Checked)
            {
                if (VentanaMensaje.ventanaConfirm("Desea dejar sin Vigencia el contrato?"))
                {
                    if (contrato.Eliminar(contrato))
                    {
                        VentanaMensaje.ventana("Contrato ya no esta vigente");
                        this.Limpiar();
                    }
                    else
                    {
                        VentanaMensaje.ventana("Problema");
                    }
                }
            }else
            {
                VentanaMensaje.ventana("El contrato ya esta sin vigencia");
            }
            
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            btnNuevoContrato.Enabled = true;
            this.txtApellidoCliente.Text = "";
            this.txtContrato.Text = "";
            this.txtDigito.Text = "";
            this.txtNombre.Text = "";
            this.txtObs.Text = "";
            this.txtPoliza.Text = "";
            this.txtPrimaAnual.Text = "";
            this.txtPrimaMensual.Text = "";
            this.txtRutCliente.Text = "";
            comboPlan.SelectedIndex = -1;
            checkDeclaracion.Checked = false;
            btnBuscarCliente.Enabled = true;
            btnEditarContrato.Enabled = false;
            btnEliminarContrato.Enabled = false;
        }

        private void btnEditarContrato_Click(object sender, EventArgs e)
        {
            Validar valida = new Validar();
            string day = DateTime.Now.ToString("dd");
            string mes = DateTime.Now.ToString("MM");
            string year = DateTime.Now.ToString("yyyy");
            string hora = DateTime.Now.ToString("hh");
            string minuto = DateTime.Now.ToString("mm");
            string segundo = DateTime.Now.ToString("ss");
            DateTime thisDay = DateTime.Today;

            string a = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(comboFechaInicio.Value.ToString()));

            string m = valida.DiferenciaFechas_mes(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(comboFechaInicio.Value.ToString()));

            string d = valida.DiferenciaFechas_dia(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(comboFechaInicio.Value.ToString()));

            if (a.Equals("Fecha Invalida") || m.Equals("Fecha Invalida"))
            {

                string a1 = valida.DiferenciaFechas_anio(DateTime.Parse(comboFechaInicio.Value.ToString()), DateTime.Parse(thisDay.ToString("d")));

                string a2 = valida.DiferenciaFechas_mes(DateTime.Parse(comboFechaInicio.Value.ToString()), DateTime.Parse(thisDay.ToString("d")));

                string a3 = valida.DiferenciaFechas_dia(DateTime.Parse(comboFechaInicio.Value.ToString()), DateTime.Parse(thisDay.ToString("d")));

                if (Convert.ToInt32(a1) > 0)
                {
                    VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                    return;
                }
                else
                {
                    if (Convert.ToInt32(a2) > 0)
                    {
                        if (Convert.ToInt32(a2) == 1)
                        {
                            if (Convert.ToInt32(a3) == 0)
                            {

                            }
                            else
                            {
                                VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                            }
                        }
                        else
                        {
                            VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                        }
                    }
                    else
                    {

                    }
                }
            }
            else
            {

                if (Convert.ToInt32(a) == 0 && Convert.ToInt32(m) == 0 && Convert.ToInt32(d) == 0)
                {

                }
                else
                {
                    VentanaMensaje.ventana("La fecha no puede ser menor a la actual");
                    return;
                }
            }






            if (this.txtRutCliente.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Seleccionar un Cliente");
                return;
            }

            if (this.comboPlan.SelectedIndex == -1 || this.comboPlan.SelectedIndex == 0)
            {
                VentanaMensaje.ventana("Debe Seleccionar un Plan");
                return;
            }


            List<Cliente> listcli = new List<Cliente>();
            Contrato contrato = new Contrato();
            contrato.Numero = this.txtContrato.Text.Trim();
            //contrato.RutCliente = this.txtRutCliente.Text + "-" + this.txtDigito.Text;
            //contrato.FechaCreacion = DateTime.Today;
            contrato.CodigoPlan = "VID0" + this.comboPlan.SelectedIndex;
            contrato.FechaInicioVigencia = this.comboFechaInicio.Value;
            //contrato.FechaFinVigencia = this.comboFechaFinal.Value;
            //contrato.Vigencia = Convert.ToBoolean(this.checkVigencia.Checked ? 1 : 0);
            contrato.DeclaracionSalud = Convert.ToBoolean(this.checkDeclaracion.Checked ? 1 : 0);
            contrato.PrimaAnual = Convert.ToSingle(this.txtPrimaAnual.Text);
            contrato.PrimaMensual = Convert.ToSingle(this.txtPrimaMensual.Text);
            contrato.Observaciones = txtObs.Text.Trim();
            
            Cliente cliente = new Cliente();
            cliente.RutCliente = txtRutCliente + "-" + txtDigito;
            listcli = contrato.BuscarCliente(cliente);

            if (contrato.ActualizarContrato(contrato))
            {
                VentanaMensaje.ventana("Contrato Actualizado correctamente ");
                this.Limpiar();
            }
            else
            {
                VentanaMensaje.ventana("Problema actualizando los datos. Vuelva a intentarlo en un momento !! c", "Advertencia!");
            }

        }
    }
}
