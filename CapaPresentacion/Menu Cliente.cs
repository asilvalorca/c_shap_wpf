﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using CapaNegocio;


namespace CapaPresentacion
{
    public partial class Menu_Cliente : Form, IFormCliente
    {
        DateTime thisDay = DateTime.Today;
        public Menu_Cliente()
        {
            InitializeComponent();
            Sexo sexo = new Sexo();

           this.comboSexo.Items.Add("Debe Seleccionar Sexo");
            List<Sexo> listSexo = sexo.ListarSexos();
            foreach (Sexo obj in listSexo)
            {

                this.comboSexo.Items.Add(obj.Descripcion);
            }
            EstadoCivil estadoCivil = new EstadoCivil();
            List<EstadoCivil> listEstadoCivil = estadoCivil.ListarEstadoCiviles();
            this.comboEstadoCivil.Items.Add("Debe Seleccionar Estado Civil");
            foreach (EstadoCivil obj in listEstadoCivil)
            {

                this.comboEstadoCivil.Items.Add(obj.Descripcion);
            }


        }
        public void CargarContratoEncontrado(Contrato co)
        {
        }
        public void CargarClienteEncontrado(Cliente cli)
        {
            //VentanaMensaje.ventana("paso");
            this.btnNuevoCliente.Enabled = false;
            this.btnEditarCliente.Enabled = true;
            this.btnEliminarCliente.Enabled = true;
            string input = cli.RutCliente;
            string pattern = "-";            // Split on hyphens

            string[] substrings = Regex.Split(input, pattern);

            btnNuevoCliente.Enabled = false;
            this.txtRutCliente.Text = substrings[0];
            this.txtDigito.Text = substrings[1];
            this.txtNombreCliente.Text = cli.Nombres;
            this.txtApellidoCliente.Text = cli.Apellidos;
            this.comboEstadoCivil.SelectedIndex = cli.IdEstadoCivil;
            this.comboSexo.SelectedIndex = cli.IdSexo;
            this.ComboFechaNac.Value = cli.FechaNacimiento;
            this.rutRecuperacion.Text = cli.RutCliente;
           // btnEliminarCliente.Enabled = false;


        }
        public void limpiar()
        {
            this.txtRutCliente.Text = "";
            this.btnNuevoCliente.Enabled = true;
            this.txtNombreCliente.Text = "";
            this.txtApellidoCliente.Text = "";
            this.txtDigito.Text = "";
            this.comboEstadoCivil.SelectedIndex = -1;
            this.comboSexo.SelectedIndex = -1;
            this.rutRecuperacion.Text = "";
        }

        public void CargarDatosUsuario()
        {
            
            
            if (this.txtRutCliente.Text.Trim() != "" && this.txtDigito.Text.Trim() != "")
            {
                
                Cliente cli = new Cliente();
                cli.RutCliente = this.txtRutCliente.Text.Trim() + "-" + this.txtDigito.Text.Trim();
                List<Cliente> listCliente = new List<Cliente>();
                listCliente = cli.ListarClienteId(cli);
                if (listCliente != null && listCliente.Count > 0)
                {
                    this.btnNuevoCliente.Enabled = false;
                    foreach (Cliente c in listCliente)
                    {
                        this.rutRecuperacion.Text = c.RutCliente;
                        this.txtNombreCliente.Text = c.Nombres;
                        this.txtApellidoCliente.Text = c.Apellidos;
                        this.comboEstadoCivil.SelectedIndex = c.IdEstadoCivil;
                        this.comboSexo.SelectedIndex = c.IdSexo;
                        this.ComboFechaNac.Value = c.FechaNacimiento;
                    }
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 m = new Form1();
            m.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 m = new Form1();
            m.Show();
        }

        private void btnNuevoCliente_Click(object sender, EventArgs e)
        {
            Cliente cliente = new Cliente();
            Validar valida = new Validar();
           

            //VentanaMensaje.ventana("" + this.comboSexo.SelectedIndex);
            //VentanaMensaje.ventana("" + this.comboEstadoCivil.SelectedIndex);
            //return;


            if (this.txtRutCliente.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el rut", "Advertencia!");
                return;
            }

            if (this.txtDigito.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Digito Verificador", "Advertencia!");
                return;
            }

            if (this.txtNombreCliente.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Nombre del Cliente", "Advertencia!");
                return;
            }

            if (this.txtApellidoCliente.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Apellido del Cliente", "Advertencia!");
                return;
            }
            if (this.comboSexo.SelectedIndex == 0 || this.comboSexo.SelectedIndex == -1)
            {
                VentanaMensaje.ventana("Debe Seleccionar el Sexo", "Advertencia!");
                return;
            }
            if (this.comboEstadoCivil.SelectedIndex == 0 || this.comboEstadoCivil.SelectedIndex == -1)
            {
                VentanaMensaje.ventana("Debe Seleccionar el Estado Civil", "Advertencia!");
                return;
            }
            if (ComboFechaNac.Value.Date.ToString() == "")
            {
                VentanaMensaje.ventana("Debe Seleccionar fecha de cumpleaños", "Advertencia!");
                return;
            }

            string str = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(ComboFechaNac.Value.Date.ToString()));
            int edad = Convert.ToInt32(str);
            if (edad < 18)
            {
                VentanaMensaje.ventana("El Cliente debe ser mayor de edad", "Advertencia!");
                return;
            }


            cliente.RutCliente = this.txtRutCliente.Text.Trim() + "-" + this.txtDigito.Text.Trim().ToUpper();
            cliente.Nombres = this.txtNombreCliente.Text.Trim().ToUpper();
            cliente.Apellidos = this.txtApellidoCliente.Text.Trim().ToUpper();
            cliente.FechaNacimiento = DateTime.Parse(ComboFechaNac.Value.Date.ToString());
            cliente.IdEstadoCivil = this.comboEstadoCivil.SelectedIndex;
            cliente.IdSexo = this.comboSexo.SelectedIndex;

            List<Cliente> cli = new List<Cliente>();
            cli = cliente.ListarClienteId(cliente);
            if (cli != null)
            {
                if (cli.Count == 0)
                {
                    if (cliente.Agregar(cliente))
                    {
                        VentanaMensaje.ventana("Cliente ingresado correctamente ", "Advertencia!");
                        this.limpiar();
                    }
                    else
                    {
                        VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momento !! c", "Advertencia!");
                    }
                }
                else if (cli.Count > 0)
                {
                    VentanaMensaje.ventana("El usuario ya existe, ingrese otro usuario !! b", "Advertencia!");

                }

            }
            else
            {
                VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momemnto!! ", "Advertencia!");
            }

        }

       

        private void label7_Click_1(object sender, EventArgs e)
        {

        }

       
      
        private void txtRutCliente_TextChanged(object sender, EventArgs e)
        {
            txtRutCliente.Text = Validar.ValidaNumero(txtRutCliente.Text);
            txtRutCliente.Select(txtRutCliente.Text.Length, 0);
        }
       

       

        private void txtDigito_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtDigito_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            ListarUsuario lu = new ListarUsuario();
            lu.Show(this);
            lu.LoadAllUsers();
        }

        private void txtRut_Leave(object sender, EventArgs e)
        {
            this.CargarDatosUsuario();
        }

        private void txtDigito_Leave(object sender, EventArgs e)
        {
            this.CargarDatosUsuario();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.limpiar();
        }

        private void Eliminar_Click(object sender, EventArgs e)
        {
            if (VentanaMensaje.ventanaConfirm("Esta seguro que desea eliminar al Cliente?"))
            {

                Contrato c = new Contrato();
                Cliente cli = new Cliente();
                cli.RutCliente = this.txtRutCliente.Text.Trim() + "-" + this.txtDigito.Text.Trim().ToUpper();
             
                List<Cliente> listCliente = new List<Cliente>();
                listCliente = c.BuscarCliente(cli);
                if (listCliente != null)
                {
                    if (listCliente.Count > 0)
                    {
                        VentanaMensaje.ventana("No se puede eliminar un Cliente que tiene plan");
                    }
                    else
                    {
                        if (cli.DeleteCliente(cli))
                        {
                            VentanaMensaje.ventana("Cliente fue eliminado con exito");
                            this.limpiar();
                        }
                        else
                        {
                            VentanaMensaje.ventana("Hay un problema eliminando el usuario!!, vuelva a intentarlo");
                        }
                    }
                }
                else
                {
                    VentanaMensaje.ventana("Hay un problema eliminando el usuario!, vuelva a intentarlo");
                }

            }
            
        }

        private void btnEditarCliente_Click(object sender, EventArgs e)
        {
            if (VentanaMensaje.ventanaConfirm("Desea Editar el Cliente ?"))
            {
                Cliente cliente = new Cliente();
                Validar valida = new Validar();
                //DateTime thisDay = DateTime.Today;





                if (this.txtRutCliente.Text.Trim() == "")
                {
                    VentanaMensaje.ventana("Debe Ingresar el rut", "Advertencia!");
                    return;
                }

                if (this.txtDigito.Text.Trim() == "")
                {
                    VentanaMensaje.ventana("Debe Ingresar el Digito Verificador", "Advertencia!");
                    return;
                }

                if (this.txtNombreCliente.Text.Trim() == "")
                {
                    VentanaMensaje.ventana("Debe Ingresar el Nombre del Cliente", "Advertencia!");
                    return;
                }

                if (this.txtApellidoCliente.Text.Trim() == "")
                {
                    VentanaMensaje.ventana("Debe Ingresar el Apellido del Cliente", "Advertencia!");
                    return;
                }
                if (this.comboSexo.SelectedIndex == 0 || this.comboSexo.SelectedIndex == -1)
                {
                    VentanaMensaje.ventana("Debe Seleccionar el Sexo", "Advertencia!");
                    return;
                }
                if (this.comboEstadoCivil.SelectedIndex == 0 || this.comboEstadoCivil.SelectedIndex == -1)
                {
                    VentanaMensaje.ventana("Debe Seleccionar el Estado Civil", "Advertencia!");
                    return;
                }
                if (ComboFechaNac.Value.Date.ToString() == "")
                {
                    VentanaMensaje.ventana("Debe Seleccionar fecha de cumpleaños", "Advertencia!");
                    return;
                }

                string str = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(ComboFechaNac.Value.Date.ToString()));
                int edad = Convert.ToInt32(str);
                if (edad < 18)
                {
                    VentanaMensaje.ventana("El Cliente debe ser mayor de edad", "Advertencia!");
                    return;
                }

                Contrato c = new Contrato();
                Cliente cli = new Cliente();
                
                
                cli.Nombres = this.txtNombreCliente.Text.Trim().ToUpper();
                cli.Apellidos = this.txtApellidoCliente.Text.Trim().ToUpper();
                cli.FechaNacimiento = DateTime.Parse(ComboFechaNac.Value.Date.ToString());
                cli.IdEstadoCivil = this.comboEstadoCivil.SelectedIndex;
                cli.IdSexo = this.comboSexo.SelectedIndex;
                if (this.rutRecuperacion.Text.Trim()=="")
                {
                    cli.RutCliente = this.txtRutCliente.Text.Trim() + "-" + this.txtDigito.Text.Trim().ToUpper();
                }else
                {
                    cli.RutCliente = this.rutRecuperacion.Text.Trim();
                }
                List<Cliente> listCliente = new List<Cliente>();
                listCliente = c.BuscarCliente(cli);
                if (listCliente != null)
                {
                    if (listCliente.Count > 0)
                    {
                        VentanaMensaje.ventana("No se puede editar un Cliente que tiene plan");
                    }
                    else
                    {
                       
                        cli.RutCliente = this.txtRutCliente.Text.Trim() + "-" + this.txtDigito.Text.Trim().ToUpper();
                        

                        if (cli.ActualizarCliente(cli, this.rutRecuperacion.Text.Trim()))
                        {
                            VentanaMensaje.ventana("Cliente fue modificado con exito");
                            this.limpiar();
                        }
                        else
                        {
                            VentanaMensaje.ventana("Hay un problema modificando el usuario!!, vuelva a intentarlo");
                        }
                    }
                }
                else
                {
                    VentanaMensaje.ventana("Hay un problema modificando el usuario!, vuelva a intentarlo");
                }

            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            txtNombreCliente.Text = Validar.ValidaLetras(txtNombreCliente.Text);
            txtNombreCliente.Select(txtNombreCliente.Text.Length, 0);
        }

        private void txtApellido_TextChanged(object sender, EventArgs e)
        {
            txtApellidoCliente.Text = Validar.ValidaLetras(txtApellidoCliente.Text);
            txtApellidoCliente.Select(txtApellidoCliente.Text.Length, 0);
        }
    }
}
