﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CalcularEdad : TempladeMethod
    {
        public CalcularEdad()
        {
        }
       
        public override double sumar(double recargo, int edad)
        {
            //VentanaMensaje.ventana("recargoooo edads"+ recargo);
            if (edad >= 18 && edad <= 25)
            {
                recargo = recargo + 3.6;
            }
            else if (edad >= 26 && edad <= 45)
            {
                recargo = recargo + 2.4;
            }
            else if (edad > 45)
            {
                recargo = recargo + 6.0;
            }

            return recargo;
        }
    }
}
