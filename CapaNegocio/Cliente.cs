﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Sql;

namespace CapaNegocio
{
    public class Cliente
    {
        public Conexion2 conexionSql;
        private string rutCliente;
        private string nombres;
        private string apellidos;
        private DateTime fechaNacimiento;
        private int idSexo;
        private int idEstadoCivil;
        private string sexo;
        private string estadoCivil;
        BeLifeEntities bdd = new BeLifeEntities();

        public string RutCliente
        {
            get
            {
                return rutCliente;
            }

            set
            {
                rutCliente = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public DateTime FechaNacimiento
        {
            get
            {
                return fechaNacimiento;
            }

            set
            {
                fechaNacimiento = value;
            }
        }

        public int IdSexo
        {
            get
            {
                return idSexo;
            }

            set
            {
                idSexo = value;
            }
        }

        public int IdEstadoCivil
        {
            get
            {
                return idEstadoCivil;
            }

            set
            {
                idEstadoCivil = value;
            }
        }

        public string Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public string EstadoCivil
        {
            get
            {
                return estadoCivil;
            }

            set
            {
                estadoCivil = value;
            }
        }

        public Cliente()
        {
            conexionSql = new Conexion2();
        }

        public Cliente(string rutCliente, string nombres, string apellidos, DateTime fechaNacimiento, int idSexo, int idEstadoCivil, string sexo, string estadoCivil)
        {
            this.rutCliente = rutCliente;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.fechaNacimiento = fechaNacimiento;
            this.idSexo = idSexo;
            this.idEstadoCivil = idEstadoCivil;
            this.sexo = sexo;
            this.estadoCivil = estadoCivil;
            conexionSql = new Conexion2();
        }

        public bool Agregar(Cliente c)
        {
            /*
             CapaDatos.Cliente cliente = new CapaDatos.Cliente();
            cliente.RutCliente = c.RutCliente;
            cliente.Nombres = c.Nombres;
            cliente.Apellidos = c.Apellidos;
            cliente.FechaNacimiento = c.FechaNacimiento;
            cliente.IdSexo = c.IdSexo;
            cliente.IdEstadoCivil = c.IdEstadoCivil;
            CapaDatos.BeLifeEntities bdd = new CapaDatos.BeLifeEntities();
            bdd.Cliente.Add(cliente);

            bdd.SaveChanges();
            */
            try
            {
                CapaDatos.Cliente cliente = new CapaDatos.Cliente()
                {
                    Nombres = c.Nombres,
                    Apellidos = c.Apellidos,
                    RutCliente = c.RutCliente,
                    FechaNacimiento = c.FechaNacimiento,
                    IdSexo = c.IdSexo,
                    IdEstadoCivil = c.IdEstadoCivil

                };
                bdd.Cliente.Add(cliente);
                bdd.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public List<Cliente> listarClientes()
        {
            var query = (from e in bdd.Cliente
                         select new Cliente
                         {
                             RutCliente = e.RutCliente,
                             Nombres = e.Nombres,
                             Apellidos = e.Apellidos,
                             FechaNacimiento = e.FechaNacimiento,
                             idEstadoCivil = e.IdEstadoCivil,
                             idSexo = e.IdSexo
                         });
            return query.ToList();
        }
        public List<Cliente> ListarClienteId(Cliente c)
        {
            try
            {
                var query = (from e in bdd.Cliente
                             join p in bdd.Sexo
                             on e.IdSexo equals p.IdSexo
                             join es in bdd.EstadoCivil
                             on e.IdEstadoCivil equals es.IdEstadoCivil
                             where e.RutCliente == c.RutCliente
                             select new Cliente
                             {
                                 RutCliente = e.RutCliente,
                                 Nombres = e.Nombres,
                                 Apellidos = e.Apellidos,
                                 FechaNacimiento = e.FechaNacimiento,
                                 idEstadoCivil = e.IdEstadoCivil,
                                 idSexo = e.IdSexo,
                                 Sexo = p.Descripcion,
                                 EstadoCivil = es.Descripcion
                             });
                return query.ToList();
            }
            catch
            {
                return null;
            }
           
        }

        public List<Cliente> ListarClienteDetalle()
        {
            try
            {
                var cliente = (from e in bdd.Cliente
                               join p in bdd.Sexo
                               on e.IdSexo equals p.IdSexo
                               join es in bdd.EstadoCivil
                               on e.IdEstadoCivil equals es.IdEstadoCivil
                               select new Cliente
                               {
                                   RutCliente = e.RutCliente,
                                   Nombres = e.Nombres,
                                   Apellidos = e.Apellidos,
                                   Sexo = p.Descripcion,
                                   EstadoCivil = es.Descripcion,
                                   fechaNacimiento = e.FechaNacimiento
                                   
                               }).ToList();
                return cliente;
            }
            catch
            {
                return null;
            }
            
            /*
            
            foreach (var p in cliente)
            {

                VentanaMensaje.ventana( p.Nombres+" "+ p.Apellidos + " " + p.sexo,"avertencia");
                
            }
            */
            
            
        }

        public List<Cliente> ListarClienteDetalleOpcion(String r, int s, int estc)
        {
           //VentanaMensaje.ventana("aa r:" + r + " s:" + s + " estc:" + estc);
            try
            {
                if (r.Trim() != "" && (s != -1 && s != 0) && (estc != -1 && estc != 0))
                {
                   // VentanaMensaje.ventana("aa r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where (e.RutCliente.Contains(r) && es.IdEstadoCivil == estc && p.IdSexo == s)
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();
                    return cliente;

                }
                else if (r.Trim() != "" && (s != -1 && s != 0) && (estc == -1 || estc == 0))
                {
                    //VentanaMensaje.ventana("bb r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where (e.RutCliente.Contains(r) &&  p.IdSexo == s)
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();
                    return cliente;
                }
                else if (r.Trim() != "" && (s == -1 || s == 0) && (estc != -1 && estc != 0))
                {
                    //VentanaMensaje.ventana("cc r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where (e.RutCliente.Contains(r) && es.IdEstadoCivil == estc)
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();
                    return cliente;
                }
                else if (r.Trim() != "" && (s == -1 || s == 0) && (estc == -1 || estc == 0))
                {
                    //VentanaMensaje.ventana("dd r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where (e.RutCliente== r)
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();

                    
                    return cliente;
                }

                else if (r.Trim() == "" && (s != -1 && s != 0) && (estc != -1 && estc != 0))
                {
                   // VentanaMensaje.ventana("ee r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where ( es.IdEstadoCivil == estc && p.IdSexo == s)
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();
                    return cliente;
                }
                else if (r.Trim() == "" && (s != -1 && s != 0) && (estc == -1 || estc == 0))
                {
                  //  VentanaMensaje.ventana("ff r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where ( p.IdSexo == s)
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();
                    return cliente;
                }
                else if (r.Trim() == "" && (s == -1 || s == 0) && (estc != -1 && estc != 0))
                {
                   // VentanaMensaje.ventana("gg r:" + r + " s:" + s + " estc:" + estc);
                    var cliente = (from e in bdd.Cliente
                                   join p in bdd.Sexo
                                   on e.IdSexo equals p.IdSexo
                                   join es in bdd.EstadoCivil
                                   on e.IdEstadoCivil equals es.IdEstadoCivil
                                   where ( es.IdEstadoCivil == estc )
                                   select new Cliente
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombres = e.Nombres,
                                       Apellidos = e.Apellidos,
                                       Sexo = p.Descripcion,
                                       EstadoCivil = es.Descripcion,
                                       fechaNacimiento = e.FechaNacimiento
                                   }).ToList();
                         return cliente;
                }else
                {
                   // VentanaMensaje.ventana("hh r:" + r + " s:" + s + " estc:" + estc);
                    return null;
                }
               
               
            }
            catch
            {
                return null;
            }
        }

        public List<Cliente> ListarClienteId_like(Cliente c)
        {
            var query = (from e in bdd.Cliente
                         where e.RutCliente.Contains( c.RutCliente)
                         select new Cliente
                         {
                             RutCliente = e.RutCliente,
                             Nombres = e.Nombres,
                             Apellidos = e.Apellidos,
                             FechaNacimiento = e.FechaNacimiento,
                             idEstadoCivil = e.IdEstadoCivil,
                             idSexo = e.IdSexo
                         });
            return query.ToList();
        }
        public bool ActualizarCliente(Cliente c, string r)
        {
            try
            {
                if (r == "")
                {
                    r = c.rutCliente;
                }
                //VentanaMensaje.ventana(c.r);
                //VentanaMensaje.ventana(c.rutCliente);
                CapaDatos.Cliente clienteEntity = (from e in bdd.Cliente
                                                   where e.RutCliente == r
                                                   select e).FirstOrDefault();

                clienteEntity.Nombres = c.Nombres;
                clienteEntity.Apellidos = c.Apellidos;
                clienteEntity.FechaNacimiento = c.FechaNacimiento;
                clienteEntity.IdEstadoCivil = c.IdEstadoCivil;
                clienteEntity.IdSexo = c.idSexo;
                clienteEntity.RutCliente = c.RutCliente;
                //bdd.Entry(clienteEntity).CurrentValues.SetValues(c);
                bdd.SaveChanges();

                return true;


            }
            catch
            {
                return false;
            }

        }
        public bool DeleteCliente(Cliente c)
        {
            try
            {

                CapaDatos.Cliente clienteEntity = (from e in bdd.Cliente
                                                   where e.RutCliente == c.RutCliente
                                                   select e).FirstOrDefault();
                bdd.Cliente.Remove(clienteEntity);
                bdd.SaveChanges();
                return true;


            }
            catch 
            {
                return false;
            }
        }
        public bool ActualizarClientebd(Cliente c, string r)
        {
            try
            {
                if (r == "")
                {
                    r = c.rutCliente;
                }
                if (conexionSql.AbrirConexion())
                {
                    System.IO.File.WriteAllText(@"C:\c\query.txt", string.Format("update Cliente set RutCliente = '" + c.rutCliente + "', Nombres = '" + c.Nombres + "', Apellidos = '" + c.Apellidos + "', FechaNacimiento = '" + c.FechaNacimiento + "', IdSexo = '" + c.IdSexo + "',IdEstadoCivil = '" + c.idEstadoCivil + "' WHERE RutCliente = '" + r + "'", Environment.NewLine));
                    VentanaMensaje.ventana("update Cliente set RutCliente = '" + c.rutCliente + "', Nombres = '" + c.Nombres + "', Apellidos = '" + c.Apellidos + "', FechaNacimiento = '" + c.FechaNacimiento + "', IdSexo = '" + c.IdSexo + "',IdEstadoCivil = '" + c.idEstadoCivil + "' WHERE RutCliente = '" + r + "'");
                    string cadena = "update Cliente set  Nombres = '" + c.Nombres +"', Apellidos = '" + c.Apellidos + "', FechaNacimiento = '" + c.FechaNacimiento + "', IdSexo = '" + c.IdSexo + "',IdEstadoCivil = '" + c.idEstadoCivil + "' WHERE RutCliente = '" + r + "'";
                    SqlCommand comando = new SqlCommand(cadena, conexionSql.con);

                    int cant;
                    cant = comando.ExecuteNonQuery();
                    if (cant == 1)
                    {
                       
                        return true;
                    }
                    else
                    {
                        
                        conexionSql.con.Close();
                        return false;
                    }
                        
              
                    
                }
                else
                {
                    return false;
                }
                  
                


            }
            catch
            {
                return false;
            }

        }
    }
}
