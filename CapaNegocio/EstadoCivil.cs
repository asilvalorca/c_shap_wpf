﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
namespace CapaNegocio
{
    public class EstadoCivil
    {
        private int idEstadoCivil;
        private string descripcion;
        BeLifeEntities bdd = new BeLifeEntities();
        public int IdEstadoCivil
        {
            get
            {
                return idEstadoCivil;
            }

            set
            {
                idEstadoCivil = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }
        public EstadoCivil()
        {

        }
        public EstadoCivil(int idEstadoCivil, string descripcion)
        {
            this.idEstadoCivil = idEstadoCivil;
            this.descripcion = descripcion;
        }

        public void Agregar(EstadoCivil e)
        {


            CapaDatos.EstadoCivil estadoCivil = new CapaDatos.EstadoCivil()
            {
                IdEstadoCivil = e.IdEstadoCivil,
                Descripcion = e.descripcion

            };
            bdd.EstadoCivil.Add(estadoCivil);
            bdd.SaveChanges();

        }
        public List<EstadoCivil> ListarEstadoCiviles()
        {
            var query = (from e in bdd.EstadoCivil
                         select new EstadoCivil
                         {
                             IdEstadoCivil = e.IdEstadoCivil,
                             Descripcion = e.Descripcion
                         });
            return query.ToList();
        }
        public List<EstadoCivil> ListarSexoId(EstadoCivil ec)
        {
            var query = (from e in bdd.EstadoCivil
                         where e.IdEstadoCivil == ec.IdEstadoCivil
                         select new EstadoCivil
                         {

                             idEstadoCivil = ec.IdEstadoCivil,
                             Descripcion = e.Descripcion
                         });
            return query.ToList();
        }
        public List<EstadoCivil> ListarEstadoCivilId_like(EstadoCivil ec)
        {
            var query = (from e in bdd.EstadoCivil
                         where e.Descripcion.Contains(ec.Descripcion)
                         select new EstadoCivil
                         {
                             IdEstadoCivil = ec.IdEstadoCivil,
                             Descripcion = ec.Descripcion
                         });
            return query.ToList();
        }
        public bool ActualizarEstadoCivil(EstadoCivil ec)
        {
            try
            {

                CapaDatos.EstadoCivil estadoCivilEntity = (from e in bdd.EstadoCivil
                                                where e.IdEstadoCivil == ec.idEstadoCivil
                                                select e).FirstOrDefault();

                estadoCivilEntity.Descripcion = ec.Descripcion;

                //bdd.Entry(clienteEntity).CurrentValues.SetValues(c);
                bdd.SaveChanges();

                return true;


            }
            catch 
            {
                return false;
            }

        }
        public bool BorrarEstadocivil(EstadoCivil ec)
        {
            try
            {

                CapaDatos.EstadoCivil estadoCivilEntity = (from e in bdd.EstadoCivil
                                             where e.IdEstadoCivil == ec.idEstadoCivil
                                             select e).FirstOrDefault();
                bdd.EstadoCivil.Remove(estadoCivilEntity);
                bdd.SaveChanges();
                return true;


            }
            catch 
            {
                return false;
            }
        }
    }
}
