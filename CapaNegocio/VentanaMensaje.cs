﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaNegocio
{
    public class VentanaMensaje
    {
       public VentanaMensaje()
        {

        }
        public static void ventana(string mensaje, string titulo)
        {
            MessageBox.Show(mensaje, titulo, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        public static void ventana(string mensaje)
        {
            MessageBox.Show(mensaje, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        

        public static bool ventanaConfirm(string mensaje)
        {
            if (MessageBox.Show(mensaje, "Advertencia",
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                   == DialogResult.Yes)
            {
                return true;
            }else
            {
                return false;
            }
        }

        public static bool ventanaConfirm(string mensaje, string titulo)
        {
            if (MessageBox.Show(mensaje, titulo,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                  == DialogResult.Yes)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
