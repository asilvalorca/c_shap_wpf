﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CapaNegocio
{
    public class Contrato
    {
        private string numero;
        private DateTime fechaCreacion;
        private DateTime fechaTermino;
        private string rutCliente;
        private string codigoPlan;
        private DateTime fechaInicioVigencia;
        private DateTime fechaFinVigencia;
        private bool vigencia;
        private bool declaracionSalud;
        private double primaAnual;
        private double primaMensual;
        private string observaciones;

        private string nombre;
        private string apellido;
        private string plan;
        private string poliza;
        private DateTime fechaNacimiento;
        BeLifeEntities bdd = new BeLifeEntities();
        public Conexion2 conexionSql;


        public string Numero
        {
            get
            {
                return numero;
            }

            set
            {
                numero = value;
            }
        }

        public DateTime FechaCreacion
        {
            get
            {
                return fechaCreacion;
            }

            set
            {
                fechaCreacion = value;
            }
        }

        public DateTime FechaTermino
        {
            get
            {
                return fechaTermino;
            }

            set
            {
                fechaTermino = value;
            }
        }

        public string RutCliente
        {
            get
            {
                return rutCliente;
            }

            set
            {
                rutCliente = value;
            }
        }

        public string CodigoPlan
        {
            get
            {
                return codigoPlan;
            }

            set
            {
                codigoPlan = value;
            }
        }

        public DateTime FechaInicioVigencia
        {
            get
            {
                return fechaInicioVigencia;
            }

            set
            {
                fechaInicioVigencia = value;
            }
        }

        public DateTime FechaFinVigencia
        {
            get
            {
                return fechaFinVigencia;
            }

            set
            {
                fechaFinVigencia = value;
            }
        }

        public bool Vigencia
        {
            get
            {
                return vigencia;
            }

            set
            {
                vigencia = value;
            }
        }

        public bool DeclaracionSalud
        {
            get
            {
                return declaracionSalud;
            }

            set
            {
                declaracionSalud = value;
            }
        }

        public double PrimaAnual
        {
            get
            {
                return primaAnual;
            }

            set
            {
                primaAnual = value;
            }
        }

        public double PrimaMensual
        {
            get
            {
                return primaMensual;
            }

            set
            {
                primaMensual = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string Plan
        {
            get
            {
                return plan;
            }

            set
            {
                plan = value;
            }
        }

        public string Poliza
        {
            get
            {
                return poliza;
            }

            set
            {
                poliza = value;
            }
        }

        public DateTime FechaNacimiento
        {
            get
            {
                return fechaNacimiento;
            }

            set
            {
                fechaNacimiento = value;
            }
        }

        public Contrato()
        {

        }

        public Contrato(string numero, DateTime fechaCreacion, DateTime fechaTermino, string rutCliente, string codigoPlan, DateTime fechaInicioVigencia, DateTime fechaFinVigencia, bool vigencia, bool declaracionSalud, double primaAnual, double primaMensual, string observaciones)
        {
            this.numero = numero;
            this.fechaCreacion = fechaCreacion;
            this.fechaTermino = fechaTermino;
            this.rutCliente = rutCliente;
            this.codigoPlan = codigoPlan;
            this.fechaInicioVigencia = fechaInicioVigencia;
            this.fechaFinVigencia = fechaFinVigencia;
            this.vigencia = vigencia;
            this.declaracionSalud = declaracionSalud;
            this.primaAnual = primaAnual;
            this.primaMensual = primaMensual;
            this.observaciones = observaciones;
        }
        public List<Cliente> BuscarCliente(Cliente c)
        {
            try
            {
                var query = (from e in bdd.Contrato
                             where e.RutCliente == c.RutCliente
                             select new Cliente
                             {
                                 RutCliente = e.RutCliente,
                                
                             });
                return query.ToList();
            }
            catch
            {
                return null;
            }

        }
        public bool Agregar(Contrato c)
        {
           
            try
            {
                CapaDatos.Contrato contrato = new CapaDatos.Contrato()
                {
                    Numero = c.Numero,
                    FechaCreacion = c.FechaCreacion,
                    RutCliente = c.RutCliente,
                    CodigoPlan = c.CodigoPlan,
                    FechaInicioVigencia = c.FechaInicioVigencia,
                    FechaFinVigencia = c.FechaFinVigencia,
                    Vigente = c.vigencia,
                    DeclaracionSalud = c.DeclaracionSalud,
                    PrimaAnual = c.PrimaAnual,
                    PrimaMensual = c.PrimaMensual,
                    Observaciones = c.Observaciones,
                };

                bdd.Contrato.Add(contrato);
                bdd.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Contrato> ListarContratoDetalle()
        {
            try
            {
                var contrato = (from e in bdd.Cliente
                                join c in bdd.Contrato
                                on e.RutCliente equals c.RutCliente
                                join p in bdd.Plan
                                on c.CodigoPlan equals p.IdPlan
                               select new Contrato
                               {
                                   RutCliente = e.RutCliente,
                                   Nombre = e.Nombres,
                                   Apellido = e.Apellidos,
                                   Poliza = p.PolizaActual,
                                   Plan = p.Nombre,
                                   Numero = c.Numero

                               }).ToList();
                return contrato;
            }
            catch
            {
                return null;
            }
        }
        public List<Contrato> ListarContratoDetalleOpcion(String r, string n, string poliza)
        {
            //VentanaMensaje.ventana("xxxxxxx r:" + r + " n:" + n + " poliza:" + poliza);
            try
            {
                if (r.Trim() != "" && n.Trim() != "" && poliza.Trim() != "")
                {
                    //VentanaMensaje.ventana("aaaa r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                    join c in bdd.Contrato
                                    on e.RutCliente equals c.RutCliente
                                    join p in bdd.Plan
                                    on c.CodigoPlan equals p.IdPlan
                                    where (e.RutCliente.Contains(r) && c.Numero.Contains(n) && p.PolizaActual == poliza)
                                    select new Contrato
                                    {
                                        RutCliente = e.RutCliente,
                                        Nombre = e.Nombres,
                                        Apellido = e.Apellidos,
                                        Poliza = p.PolizaActual,
                                        Plan = p.Nombre,
                                        Numero = c.Numero

                                    }).ToList();
                    return contrato;

                }
                else if (r.Trim() != "" && n.Trim() != "" && poliza.Trim() == "")
                {
                   // VentanaMensaje.ventana("bbb r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                    join c in bdd.Contrato
                                    on e.RutCliente equals c.RutCliente
                                    join p in bdd.Plan
                                    on c.CodigoPlan equals p.IdPlan
                                    where (e.RutCliente.Contains(r) && c.Numero == n)
                                    select new Contrato
                                    {
                                        RutCliente = e.RutCliente,
                                        Nombre = e.Nombres,
                                        Apellido = e.Apellidos,
                                        Poliza = p.PolizaActual,
                                        Plan = p.Nombre,
                                        Numero = c.Numero

                                    }).ToList();
                    return contrato;
                }
                else if (r.Trim() != "" && n.Trim() == "" && poliza.Trim() != "")
                {
                   // VentanaMensaje.ventana("ccc r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                   join c in bdd.Contrato
                                   on e.RutCliente equals c.RutCliente
                                   join p in bdd.Plan
                                   on c.CodigoPlan equals p.IdPlan
                                    where (e.RutCliente.Contains(r) && p.PolizaActual == poliza)
                                    select new Contrato
                                   {
                                       RutCliente = e.RutCliente,
                                       Nombre = e.Nombres,
                                       Apellido = e.Apellidos,
                                       Poliza = p.PolizaActual,
                                       Plan = p.Nombre,
                                       Numero = c.Numero

                                   }).ToList();
                    return contrato;
                }
                else if (r.Trim() != "" && n.Trim() == "" && poliza.Trim() == "")
                {
                   // VentanaMensaje.ventana("ddd r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                    join c in bdd.Contrato
                                    on e.RutCliente equals c.RutCliente
                                    join p in bdd.Plan
                                    on c.CodigoPlan equals p.IdPlan
                                    where (e.RutCliente == r)
                                    select new Contrato
                                    {
                                        RutCliente = e.RutCliente,
                                        Nombre = e.Nombres,
                                        Apellido = e.Apellidos,
                                        Poliza = p.PolizaActual,
                                        Plan = p.Nombre,
                                        Numero = c.Numero

                                    }).ToList();
                    return contrato;
                }

                else if (r.Trim() == "" && n.Trim() != "" && poliza != "")
                {
                   // VentanaMensaje.ventana("eee r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                    join c in bdd.Contrato
                                    on e.RutCliente equals c.RutCliente
                                    join p in bdd.Plan
                                    on c.CodigoPlan equals p.IdPlan
                                    where (c.Numero==n  && p.PolizaActual == poliza)
                                    select new Contrato
                                    {
                                        RutCliente = e.RutCliente,
                                        Nombre = e.Nombres,
                                        Apellido = e.Apellidos,
                                        Poliza = p.PolizaActual,
                                        Plan = p.Nombre,
                                        Numero = c.Numero

                                    }).ToList();
                    return contrato;
                }
                else if (r.Trim() == "" && n != "" && poliza == "")
                {
                    //VentanaMensaje.ventana("fff r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                    join c in bdd.Contrato
                                    on e.RutCliente equals c.RutCliente
                                    join p in bdd.Plan
                                    on c.CodigoPlan equals p.IdPlan
                                    where (c.Numero == n)
                                    select new Contrato
                                    {
                                        RutCliente = e.RutCliente,
                                        Nombre = e.Nombres,
                                        Apellido = e.Apellidos,
                                        Poliza = p.PolizaActual,
                                        Plan = p.Nombre,
                                        Numero = c.Numero

                                    }).ToList();
                    return contrato;
                }
                else if (r.Trim() == "" && n == "" && poliza != "")
                {

                   // VentanaMensaje.ventana("ggg r:" + r + " n:" + n + " poliza:" + poliza);
                    var contrato = (from e in bdd.Cliente
                                    join c in bdd.Contrato
                                    on e.RutCliente equals c.RutCliente
                                    join p in bdd.Plan
                                    on c.CodigoPlan equals p.IdPlan
                                    where (p.PolizaActual == poliza)
                                    select new Contrato
                                    {
                                        RutCliente = e.RutCliente,
                                        Nombre = e.Nombres,
                                        Apellido = e.Apellidos,
                                        Poliza = p.PolizaActual,
                                        Plan = p.Nombre,
                                        Numero = c.Numero

                                    }).ToList();
                    return contrato;
                }
                else
                {
                   // VentanaMensaje.ventana("hhh r:" + r + " n:" + n + " poliza:" + poliza);
                    return null;
                }


            }
            catch
            {
                return null;
            }
        }
        public List<Contrato> ListarContratoId(Contrato ctrato)
        {
            try
            {
                var query = (from e in bdd.Cliente
                             join c in bdd.Contrato
                             on e.RutCliente equals c.RutCliente
                             join p in bdd.Plan
                             on c.CodigoPlan equals p.IdPlan
                             where (c.Numero == ctrato.Numero)
                             select new Contrato
                             {

                                 RutCliente = e.RutCliente,
                                 Nombre = e.Nombres,
                                 Apellido = e.Apellidos,
                                 Poliza = p.PolizaActual,
                                 Plan = p.Nombre,
                                 Numero = c.Numero,
                                 PrimaAnual = c.PrimaAnual,
                                 PrimaMensual = c.PrimaMensual,
                                 Observaciones = c.Observaciones,
                                 FechaInicioVigencia = c.FechaInicioVigencia,
                                 FechaFinVigencia = c.FechaFinVigencia,
                                 vigencia = c.Vigente,
                                 fechaNacimiento = e.FechaNacimiento,
                                 CodigoPlan = c.CodigoPlan,
                                 DeclaracionSalud = c.DeclaracionSalud


                             });
                foreach(Contrato q in query)
                {

                    //VentanaMensaje.ventana("No hay datos para mostrar"+q.codigoPlan);
                }
                return query.ToList();
            }
            catch
            {
                return null;
            }

        }
        public bool ActualizarContrato(Contrato c)
        {
            /*
            conexionSql = new Conexion2();
            if (conexionSql.AbrirConexion())
            {
                String query = " UPDATE [BeLife].[dbo].[Contrato] "
                    + " SET RutCliente = '" + c.RutCliente + "', "
                    + " CodigoPlan = '" + c.CodigoPlan + "', "
                    + " DeclaracionSalud = '" + c.DeclaracionSalud + "', "
                    + " PrimaMensual = '" + c.primaMensual + "', "
                    + " PrimaAnual = '" + c.primaAnual + "', "
                    + " Observaciones = '" + c.Observaciones + "',"
                    + " FechaInicioVigencia = '" + c.FechaInicioVigencia + "',"
                    + " FechaFinVigencia = '" + c.FechaFinVigencia + "'"
                    + " WHERE Numero = '"+ c.Numero+ "'";
                SqlCommand comando = new SqlCommand(query, conexionSql.con);
                comando.ExecuteNonQuery;
            }
            */
            try
            {
                // VentanaMensaje.ventana(c.rutCliente);
                CapaDatos.Contrato clienteEntity = (from e in bdd.Contrato
                                                    where e.Numero == c.Numero
                                                    select e).FirstOrDefault();

                clienteEntity.DeclaracionSalud = Convert.ToBoolean(c.DeclaracionSalud);
               // clienteEntity.CodigoPlan = c.CodigoPlan;
                clienteEntity.FechaFinVigencia = c.FechaFinVigencia;
                clienteEntity.Observaciones = c.Observaciones;
               // clienteEntity.PrimaAnual = c.PrimaAnual;
                //clienteEntity.PrimaMensual = c.PrimaMensual;
                //clienteEntity.RutCliente = c.RutCliente;
                

                //bdd.Entry(clienteEntity).CurrentValues.SetValues(c);
                bdd.SaveChanges();

                return true;


            }
            catch
            {
                return false;
            }

        }
        public bool Eliminar(Contrato c)
        {
            try
            {
                // VentanaMensaje.ventana(c.rutCliente);
                CapaDatos.Contrato clienteEntity = (from e in bdd.Contrato
                                                    where e.Numero == c.Numero
                                                    select e).FirstOrDefault();

                clienteEntity.Vigente = Convert.ToBoolean(0);
                clienteEntity.FechaFinVigencia = c.FechaFinVigencia;
               
                //bdd.Entry(clienteEntity).CurrentValues.SetValues(c);
                bdd.SaveChanges();

                return true;


            }
            catch
            {
                return false;
            }

        }
    }
}
