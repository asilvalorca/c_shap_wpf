﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
namespace CapaNegocio
{
    public class Plan
    {
        private string idPlan;
        private string nombre;
        private double primaBase;
        private string polizaActual;
        BeLifeEntities bdd = new BeLifeEntities();

        public Plan()
        {

        }
        public string IdPlan
        {
            get
            {
                return idPlan;
            }

            set
            {
                idPlan = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public double PrimaBase
        {
            get
            {
                return primaBase;
            }

            set
            {
                primaBase = value;
            }
        }

        public string PolizaActual
        {
            get
            {
                return polizaActual;
            }

            set
            {
                polizaActual = value;
            }
        }

        public Plan(string idPlan, string nombre, double primaBase, string polizaActual)
        {
            this.idPlan = idPlan;
            this.nombre = nombre;
            this.primaBase = primaBase;
            this.polizaActual = polizaActual;
        }
        public List<Plan> listarPlanes()
        {
            var query = (from e in bdd.Plan
                         select new Plan
                         {
                             IdPlan = e.IdPlan,
                             Nombre = e.Nombre,
                             PrimaBase = e.PrimaBase,
                             PolizaActual = e.PolizaActual,
                           
                         });
            return query.ToList();
        }
        public List<Plan> ListarPlanId(Plan p)
        {
            try
            {
                var query = (from e in bdd.Plan
                             where e.IdPlan == p.IdPlan
                             select new Plan
                             {
                                 IdPlan = e.IdPlan,
                                 Nombre = e.Nombre,
                                 PrimaBase = e.PrimaBase,
                                 PolizaActual = e.PolizaActual
                             });
                return query.ToList();
            }
            catch
            {
                return null;
            }

        }
        public List<Plan> ListarPlanId_like(Plan p)
        {
            var query = (from e in bdd.Plan
                         where e.IdPlan.Contains(p.IdPlan)
                         select new Plan
                         {
                             IdPlan = e.IdPlan,
                             Nombre = e.Nombre,
                             PrimaBase = e.PrimaBase,
                             PolizaActual = e.PolizaActual,
                         });
            return query.ToList();
        }
    }
}
