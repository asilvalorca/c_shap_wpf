﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CalcularEstadoCivil : TempladeMethod
    {
        public CalcularEstadoCivil()
        {
        }


        public override double sumar(double recargo, int estadocivil)
        {
            //VentanaMensaje.ventana("recargoooo estado" + recargo);
            if (estadocivil == 1)
            {
                recargo = recargo + 4.8;
            }
            else if (estadocivil == 2)
            {
                recargo = recargo + 2.4;
            }
            else if (estadocivil > 2)
            {
                recargo = recargo + 3.6;
            }

            return recargo;
        }
    }
}
