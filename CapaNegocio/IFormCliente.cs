﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public interface IFormCliente
    {
        void CargarClienteEncontrado(Cliente cli);
        void CargarContratoEncontrado(Contrato contrato);
    }
}
