﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
namespace CapaNegocio
{
   public class Sexo
    {
        private int idSexo;
        private string descripcion;
        BeLifeEntities bdd = new BeLifeEntities();

        public Sexo()
        {

        }
        public int IdSexo
        {
            get
            {
                return idSexo;
            }

            set
            {
                idSexo = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public Sexo(int idSexo, string descripcion)
        {
            this.idSexo = idSexo;
            this.descripcion = descripcion;
        }

        public void Agregar(Sexo s)
        {
           

            CapaDatos.Sexo sexo = new CapaDatos.Sexo()
            {
                IdSexo = s.IdSexo,
                Descripcion = s.descripcion

            };
            bdd.Sexo.Add(sexo);
            bdd.SaveChanges();

        }
        public List<Sexo> ListarSexos()
        {
            var query = (from e in bdd.Sexo
                         select new Sexo
                         {
                            IdSexo = e.IdSexo,
                            Descripcion = e.Descripcion
                         });
            return query.ToList();
        }
        public List<Sexo> ListarSexoId(Sexo c)
        {
            var query = (from e in bdd.Sexo
                         where e.IdSexo == c.IdSexo
                         select new Sexo
                         {
                             
                             idSexo = e.IdSexo,
                             Descripcion = e.Descripcion
                         });
            return query.ToList();
        }
        public List<Sexo> ListarSexoId_like(Sexo s)
        {
            var query = (from e in bdd.Sexo
                         where e.Descripcion.Contains(s.Descripcion)
                         select new Sexo
                         {
                             idSexo = e.IdSexo,
                             Descripcion = e.Descripcion
                         });
            return query.ToList();
        }
        public bool ActualizarCliente(Sexo s)
        {
            try
            {

                CapaDatos.Sexo sexoEntity = (from e in bdd.Sexo
                                                   where e.IdSexo == s.IdSexo
                                                   select e).FirstOrDefault();

                sexoEntity.Descripcion = s.Descripcion;
               
                //bdd.Entry(clienteEntity).CurrentValues.SetValues(c);
                bdd.SaveChanges();

                return true;


            }
            catch 
            {
                return false;
            }

        }
        public bool BorrarSexo(Sexo s)
        {
            try
            {

                CapaDatos.Sexo sexoEntity = (from e in bdd.Sexo
                                                   where e.IdSexo == s.IdSexo
                                                   select e).FirstOrDefault();
                bdd.Sexo.Remove(sexoEntity);
                bdd.SaveChanges();
                return true;


            }
            catch
            {
                return false;
            }
        }
    }
}
