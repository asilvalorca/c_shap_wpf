﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para ListaClientes.xaml
    /// </summary>
    public partial class ListaClientes : MetroWindow
    {
        public ListaClientes()
        {
            InitializeComponent();
            LoadAllUsers();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Flybuton.IsOpen = true;
        }
        public void LoadAllUsers()
        {

            Cliente cliente = new Cliente();
            List<Cliente> listCliente = new List<Cliente>();

            listCliente = cliente.ListarClienteDetalle();
            if (listCliente.Count() == 0 || listCliente == null)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }

            GridClientes.ItemsSource = listCliente;



        }
        public void LoadUser(string rut, int idsexo, int idestadocivil)
        {

            Cliente cliente = new Cliente();
            List<Cliente> listCliente = new List<Cliente>();
            listCliente = cliente.ListarClienteDetalleOpcion(rut, idsexo, idestadocivil);
            //VentanaMensaje.ventana("rut: "+rut+ "idsexo: " + idsexo + "idestadocivil: " + idestadocivil);

            if (listCliente.Count() == 0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                GridClientes.ItemsSource = null;
                return;
            }
            GridClientes.ItemsSource = listCliente;


        }
        private void btnBuscarCri_Click(object sender, RoutedEventArgs e)
        {
            string Rut = "";
            if (this.texCriRut.Text.Trim() == "" && this.texCriDigito.Text.Trim() == "")
            {

            }
            else
            {
                Rut = this.texCriRut.Text.Trim() + "-" + this.texCriDigito.Text.Trim();
            }
            if ((this.comboSexo.SelectedIndex == -1 || this.comboSexo.SelectedIndex == 0) && (this.comboEstadoCivil.SelectedIndex == -1 || this.comboEstadoCivil.SelectedIndex == 0) && this.texCriRut.Text.Trim() == "" && this.texCriDigito.Text.Trim() == "")
            {
                LoadAllUsers();
            }
            else
            {
                LoadUser(Rut, this.comboSexo.SelectedIndex, this.comboEstadoCivil.SelectedIndex);
            }
        }

        private void btnSeleccionar_Click(object sender, RoutedEventArgs e)
        {
           
            Cliente c = new Cliente();
         
            //VentanaMensaje.ventana("sin conten"+dataListarUsuario.CurrentRow.Cells[0].Value.ToString());
            try
            {

                Cliente cli = new Cliente();
                cli.RutCliente = (GridClientes.SelectedItem as Cliente).RutCliente;
                List<Cliente> listCliente = new List<Cliente>();
                listCliente = cli.ListarClienteId(cli);
                cli.Nombres = listCliente[0].Nombres;
                cli.Apellidos = listCliente[0].Apellidos;
                cli.RutCliente = listCliente[0].RutCliente;
                cli.FechaNacimiento = listCliente[0].FechaNacimiento;
                cli.IdEstadoCivil = listCliente[0].IdEstadoCivil;
                cli.IdSexo = listCliente[0].IdSexo;
                cli.Sexo = listCliente[0].Sexo;
                //VentanaMensaje.ventana(cli.Nombres);

                cli.EstadoCivil = listCliente[0].EstadoCivil;
                IFormCliente miEnlace = this.Owner as IFormCliente;
                if (miEnlace != null)
                {
                    //miEnlace.agregar ( Nombre.Text.ToString (), Apellido.Text.ToString () );
                    miEnlace.CargarClienteEncontrado(cli);
                    this.Close();
                   
                }


            }
            catch
            {

            }
        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
