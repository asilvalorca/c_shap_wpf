﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para DetalleContrato.xaml
    /// </summary>
    public partial class DetalleContrato : MetroWindow
    {
        private static DetalleContrato instancia;
        public DetalleContrato()
        {
            InitializeComponent();
        }
        public static DetalleContrato getInstancia()
        {
            if (instancia == null)
            {
                instancia = new DetalleContrato();
              // VentanaMensaje.ventana("El objeto ha sido creado");
            }
            else
            {
                //VentanaMensaje.ventana("Ya existe el objeto");
            }
            return instancia;
        }

        public void VerContrato(Contrato contrato)
        {
            txtNombre.Text = contrato.Nombre.Trim();
            txtRut.Text = contrato.RutCliente.Trim();
            txtApellidos.Text = contrato.Apellido.Trim();
            txtObservaciones.Text = contrato.Observaciones.Trim();
            txtNumContrato.Text = contrato.Numero;
            txtSalud.Text = Convert.ToBoolean(contrato.DeclaracionSalud)?"Si Declaró Salud": "No Declaró Salud";
            txtVigencia.Text = Convert.ToBoolean(contrato.Vigencia) ? "Vigente" : "No Vigente";
            txtPrimaAnual.Text = contrato.PrimaAnual.ToString();
            txtPrimaMensual.Text = contrato.PrimaMensual.ToString();
            txtPoliza.Text = contrato.Poliza;
            txtFechaInicio.Text = contrato.FechaInicioVigencia.ToShortDateString();
            txtFechaTermino.Text = contrato.FechaFinVigencia.ToShortDateString();
            txtCodigoPlan.Text = contrato.CodigoPlan;
            Cliente cliente = new Cliente();
            cliente.RutCliente = contrato.RutCliente.Trim();
            List<Cliente> listCliente = new List<Cliente>();
            listCliente = cliente.ListarClienteId(cliente);
            txtSexo.Text = listCliente[0].Sexo;
            txtFNacimiento.Text = listCliente[0].FechaNacimiento.ToShortDateString();
            txtECivil.Text = listCliente[0].EstadoCivil;

        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void cerrando(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            e.Cancel = true;
        }
    }

    
}
