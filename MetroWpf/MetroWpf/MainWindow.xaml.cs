﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            Sexo sexo = new Sexo();

          
          



            InitializeComponent();
        }

        private void btnadmCli_Click(object sender, RoutedEventArgs e)
        {
            BuElMo BuElMo = new BuElMo();
            BuElMo.Show();
            this.Close();
        }

        private void btnadmCont_Click(object sender, RoutedEventArgs e)
        {
            ListaContrato Contratos = new ListaContrato();
            Contratos.Show();
            this.Close();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
