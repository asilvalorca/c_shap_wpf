﻿#pragma checksum "..\..\DetalleContrato.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8AC37A723B6F8A7B36CEBC29C0827F2741C9D65A"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MahApps.Metro.Behaviours;
using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using MetroWpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MetroWpf {
    
    
    /// <summary>
    /// DetalleContrato
    /// </summary>
    public partial class DetalleContrato : MahApps.Metro.Controls.MetroWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 32 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile btnVolver;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupBox1;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombre;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtApellidos;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSexo;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtECivil;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFNacimiento;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRut;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupContrato;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNumContrato;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPoliza;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPrimaAnual;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPrimaMensual;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFechaInicio;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtObservaciones;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFechaTermino;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSalud;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCodigoPlan;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\DetalleContrato.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtVigencia;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MetroWpf;component/detallecontrato.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\DetalleContrato.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 20 "..\..\DetalleContrato.xaml"
            ((MetroWpf.DetalleContrato)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.cerrando);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnVolver = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 32 "..\..\DetalleContrato.xaml"
            this.btnVolver.Click += new System.Windows.RoutedEventHandler(this.btnVolver_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.groupBox1 = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 4:
            this.txtNombre = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtApellidos = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtSexo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txtECivil = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtFNacimiento = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txtRut = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.groupContrato = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 12:
            this.txtNumContrato = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.txtPoliza = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.txtPrimaAnual = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.txtPrimaMensual = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.txtFechaInicio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.txtObservaciones = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.txtFechaTermino = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.txtSalud = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.txtCodigoPlan = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.txtVigencia = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

