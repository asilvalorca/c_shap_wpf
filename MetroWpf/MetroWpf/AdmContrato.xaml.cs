﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para AdmContrato.xaml
    /// </summary>
    public partial class AdmContrato : MetroWindow
    {
        public AdmContrato()
        {
            InitializeComponent();
        }

        private void gridSplitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            MainWindow ver = new MainWindow();
            ver.Show();
            this.Close();
        }

        private void btnagreCli_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnagreContrato_Click(object sender, RoutedEventArgs e)
        {
            AgregarContrato ver = new AgregarContrato();
            ver.Show();
            this.Close();
        }

        private void btnBuscarCont_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnwhatCli_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btncorCli_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
