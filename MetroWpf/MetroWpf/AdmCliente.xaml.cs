﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para AdmCliente.xaml
    /// </summary>
    public partial class AdmCliente : MetroWindow
    {
        public AdmCliente()
        {
            InitializeComponent();
        }

        private void btnagreCli_Click(object sender, RoutedEventArgs e)
        {
            AgregarCliente ver = new AgregarCliente();
            ver.Show();
            this.Close();
            

        }

        private void btnbusCli_Click(object sender, RoutedEventArgs e)
        {
            BuElMo ver = new BuElMo();
            ver.Show();
            this.Close();
            
            
        }

        private void btncorCli_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=13&ct=1526101287&rver=6.7.6640.0&wp=MBI_SSL&wreply=https%3a%2f%2foutlook.live.com%2fowa%2f%3fnlp%3d1%26RpsCsrfState%3da5c434ae-d3f9-0f84-cf4c-6ddb36262bf3&id=292841&CBCXT=out&lw=1&fl=dob%2cflname%2cwld&cobrandid=90015");
        }

        private void btnwhatCli_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://web.whatsapp.com/");
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            MainWindow ver = new MainWindow();
            ver.Show();
            this.Close();
        }
    }
}
