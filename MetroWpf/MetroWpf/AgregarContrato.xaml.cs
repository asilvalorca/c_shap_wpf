﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;

using System.Text.RegularExpressions;
namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para AgregarContrato.xaml
    /// </summary>
    public partial class AgregarContrato : MetroWindow, IFormCliente
    {
        public AgregarContrato()
        {
            InitializeComponent();
            Plan plan = new Plan();
            ComboBoxItem cbi1 = new ComboBoxItem();
            cbi1.Background = Brushes.White;
            cbi1.Foreground = Brushes.Black;
            cbi1.Content = "Debe Seleccionar Plan";
            this.comboPlan.Items.Add(cbi1);
            
            List<Plan> listPlan = plan.listarPlanes();
            foreach (Plan obj in listPlan)
            {
                ComboBoxItem cbi2 = new ComboBoxItem();
                cbi2.Background = Brushes.White;
                cbi2.Foreground = Brushes.Black;
                cbi2.Content = obj.IdPlan;
                this.comboPlan.Items.Add(cbi2);
                // this.comboPlan.Items.Add(obj.IdPlan);
               // FF240070

            }
        }
        public void CargarClienteEncontrado(Cliente cli)
        {
           // VentanaMensaje.ventana("paso");

          
            this.txtRut.Text = cli.RutCliente;           
            this.txtNombre.Text = cli.Nombres;
            this.txtApellidos.Text = cli.Apellidos;
            this.txtSexo.Text = cli.Sexo;
            this.txtECivil.Text = cli.EstadoCivil;
            this.txtFNacimiento.Text = Convert.ToDateTime(cli.FechaNacimiento).ToString("dd/MM/yyyy");
            this.comboPlan.IsEnabled = true;
            this.vigencia.IsChecked = true;
            
           


        }

        public void CargarContratoEncontrado(Contrato contrato)
        {
        }



        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            ListaContrato ver = new ListaContrato();
            ver.Show();
            this.Close();
        }


        private void txtConNombre_MouseDoubleClick_1(object sender, MouseButtonEventArgs e)
        {
           // txtConNombre.IsReadOnly = true;
        }

        private void txtConApellidos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //txtConApellidos.IsReadOnly = true;
        }

        private void txtContSexo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void txtContECivil_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void txtConFNacimiento_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnAsignar_Click(object sender, RoutedEventArgs e)
        {
            ListaClientes ListaClientes = new ListaClientes();
            ListaClientes.Owner = this;
            ListaClientes.Show();
          
        }

        private void CodigoPlan_Change(object sender, SelectionChangedEventArgs e)
        {
            if (this.comboPlan.SelectedIndex != -1 || this.comboPlan.SelectedIndex != 0)
            {
                DateTime thisDay = DateTime.Today;
               // VentanaMensaje.ventana("si entro");
                Plan p = new Plan();
                Validar valida = new Validar();
                List<Plan> listPlan = new List<Plan>();
                p.IdPlan = "VID0" + this.comboPlan.SelectedIndex;
                listPlan = p.ListarPlanId(p);
                if (listPlan != null)
                {
                    Cliente cliente = new Cliente();
                    List<Cliente> listCliente = new List<Cliente>();
                    cliente.RutCliente = txtRut.Text.Trim();
                    cliente.ListarClienteId(cliente);
                    foreach (Cliente c in listCliente)
                    {
                        cliente.RutCliente = c.RutCliente;
                        cliente.Nombres = c.Nombres;
                        cliente.Apellidos = c.Apellidos;
                        cliente.FechaNacimiento = c.FechaNacimiento;
                        cliente.IdSexo = c.IdSexo;
                        cliente.IdEstadoCivil = c.IdEstadoCivil;
                    }
                    foreach (Plan plan in listPlan)
                    {
                        string str = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(cliente.FechaNacimiento.ToString()));
                        int edad = Convert.ToInt32(str);
                        double recargo = 0;
                        CalcularEdad CalcularEdad = new CalcularEdad();
                        CalcularEstadoCivil CalcularEstadoCivil = new CalcularEstadoCivil();
                        CalcularSexo CalcularSexo = new CalcularSexo();

                        recargo += CalcularEdad.obtener(recargo, edad);
                        VentanaMensaje.ventana("recargo" + recargo);
                        recargo += CalcularSexo.obtener(recargo, cliente.IdSexo);
                        VentanaMensaje.ventana("recargo" + recargo);
                        recargo += CalcularEstadoCivil.obtener(recargo, cliente.IdEstadoCivil);
                        VentanaMensaje.ventana("recargo" + recargo);
                      
                        /*if (edad >= 18 && edad <= 25)
                        {
                            recargo = recargo + 3.6;
                        }
                        else if (edad >= 26 && edad <= 45)
                        {
                            recargo = recargo + 2.4;
                        }
                        else if (edad > 45)
                        {
                            recargo = recargo + 6.0;
                        }

                        if (cliente.IdSexo == 1)
                        {
                            recargo = recargo + 2.4;

                        }
                        else if (cliente.IdSexo == 2)
                        {
                            recargo = recargo + 1.2;

                        }

                        if (cliente.IdEstadoCivil == 1)
                        {
                            recargo = recargo + 4.8;
                        }
                        else if (cliente.IdEstadoCivil == 2)
                        {
                            recargo = recargo + 2.4;
                        }
                        else if (cliente.IdEstadoCivil > 2)
                        {
                            recargo = recargo + 3.6;
                        }
                        */
                        txtPoliza.Text = plan.PolizaActual;
                        txtPrimaAnual.Text = (plan.PrimaBase + recargo).ToString();
                        txtPrimaMensual.Text = ((plan.PrimaBase + recargo) / 12).ToString();
                    }
                }
            }
            else
            {
                txtPoliza.Text = "";
            }
        }

        private void btnagreCon_Click(object sender, RoutedEventArgs e)
        {
            Validar valida = new Validar();
            string day = DateTime.Now.ToString("dd");
            string mes = DateTime.Now.ToString("MM");
            string year = DateTime.Now.ToString("yyyy");
            string hora = DateTime.Now.ToString("hh");
            string minuto = DateTime.Now.ToString("mm");
            string segundo = DateTime.Now.ToString("ss");
            DateTime thisDay = DateTime.Today;

            string a = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(DatePickerFechaInicio.SelectedDate.ToString()));

            string m = valida.DiferenciaFechas_mes(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(DatePickerFechaInicio.SelectedDate.ToString()));

            string d = valida.DiferenciaFechas_dia(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(DatePickerFechaInicio.SelectedDate.ToString()));

            if (a.Equals("Fecha Invalida") || m.Equals("Fecha Invalida"))
            {

                string a1 = valida.DiferenciaFechas_anio(DateTime.Parse(DatePickerFechaInicio.SelectedDate.ToString()), DateTime.Parse(thisDay.ToString("d")));

                string a2 = valida.DiferenciaFechas_mes(DateTime.Parse(DatePickerFechaInicio.SelectedDate.ToString()), DateTime.Parse(thisDay.ToString("d")));

                string a3 = valida.DiferenciaFechas_dia(DateTime.Parse(DatePickerFechaInicio.SelectedDate.ToString()), DateTime.Parse(thisDay.ToString("d")));

                if (Convert.ToInt32(a1) > 0)
                {
                    VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                    return;
                }
                else
                {
                    if (Convert.ToInt32(a2) > 0)
                    {
                        if (Convert.ToInt32(a2) == 1)
                        {
                            if (Convert.ToInt32(a3) == 0)
                            {

                            }
                            else
                            {
                                VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                            }
                        }
                        else
                        {
                            VentanaMensaje.ventana("La fecha no puede ser mayor de un mes a la actual");
                        }
                    }
                    else
                    {

                    }
                }
            }
            else
            {

                if (Convert.ToInt32(a) == 0 && Convert.ToInt32(m) == 0 && Convert.ToInt32(d) == 0)
                {

                }
                else
                {
                    VentanaMensaje.ventana("La fecha no puede ser menor a la actual");
                    return;
                }
            }






            if (this.txtRut.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Seleccionar un Cliente");
                return;
            }

            if (this.comboPlan.SelectedIndex == -1 || this.comboPlan.SelectedIndex == 0)
            {
                VentanaMensaje.ventana("Debe Seleccionar un Plan");
                return;
            }


            List<Cliente> listcli = new List<Cliente>();
            Contrato contrato = new Contrato();
            contrato.Numero = year + mes + day + hora + minuto + segundo;
            contrato.RutCliente = this.txtRut.Text.Trim();
            contrato.FechaCreacion = DateTime.Today;
            contrato.CodigoPlan = "VID0" + this.comboPlan.SelectedIndex;
            contrato.FechaInicioVigencia = DatePickerFechaInicio.DisplayDate;
            contrato.FechaFinVigencia = DatePickerFechaInicio.DisplayDate;
            string FechaCorta = DatePickerFechaInicio.DisplayDate.ToShortDateString();
            string separador = "/";
            VentanaMensaje.ventana(FechaCorta);
            string[] subFecha = Regex.Split(FechaCorta, separador);
            contrato.FechaFinVigencia = DateTime.Parse(subFecha[0] + "/" + subFecha[1] + "/" + (Convert.ToInt32(subFecha[2])  + 1)); 
            contrato.Vigencia = Convert.ToBoolean(vigencia.IsChecked);
            contrato.DeclaracionSalud = Convert.ToBoolean(salud.IsChecked);
            //contrato.FechaFinVigencia = this.comboFechaFinal.Value;
            //contrato.Vigencia = Convert.ToBoolean(this.vigencia.IsChecked ? 1 : 0);
            //contrato.DeclaracionSalud = Convert.ToBoolean(this.checkDeclaracion.Checked ? 1 : 0);
            contrato.PrimaAnual = Convert.ToSingle(this.txtPrimaAnual.Text);
            contrato.PrimaMensual = Convert.ToSingle(this.txtPrimaMensual.Text);
            contrato.Observaciones = txtObservaciones.Text.Trim();

            Cliente cliente = new Cliente();
            cliente.RutCliente = txtRut.Text.Trim();
            listcli = contrato.BuscarCliente(cliente);

            if (contrato.Agregar(contrato))
            {
                VentanaMensaje.ventana("Contrato ingresado correctamente ");
                //this.Limpiar();
               
                ListaContrato ListaContrato = new ListaContrato();
                ListaContrato.Show();
                this.Close();
            }
            else
            {
                VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momento !! c", "Advertencia!");
            }

        }
    }
}
