﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;
namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para ListaContrato.xaml
    /// </summary>
    public partial class ListaContrato : MetroWindow
    {
        public ListaContrato()
        {
            InitializeComponent();
            LoadAllContrato();

            Plan Plan = new Plan();
            ComboBoxItem cbi1 = new ComboBoxItem();
            cbi1.Background = Brushes.White;
            cbi1.Foreground = Brushes.Black;
            cbi1.Content = "Debe Seleccionar Plan";
            this.comboPoliza.Items.Add(cbi1);

            List<Plan> listPlan = new List<Plan>();
            listPlan = Plan.listarPlanes();
            foreach (Plan obj in listPlan)
            {
                //VentanaMensaje.ventana(obj.PolizaActual);
                ComboBoxItem cbi2 = new ComboBoxItem();
                cbi2.Background = Brushes.White;
                cbi2.Foreground = Brushes.Black;
                cbi2.Content = obj.PolizaActual;
                this.comboPoliza.Items.Add(cbi2);
                

            }
            
        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {

            MainWindow main = new MainWindow();
            main.Show();
            this.Close();

        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Flybuton.IsOpen = true;
        }
        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            AgregarContrato AgregarContrato = new AgregarContrato();
            AgregarContrato.Show();
            this.Close();
        }
        public void LoadAllContrato()
        {

            Contrato contrato = new Contrato();
            List<Contrato> listContrato = new List<Contrato>();
            listContrato = contrato.ListarContratoDetalle();
            if (listContrato.Count() == 0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }
            GridContratos.ItemsSource = listContrato;





        }
        public void LoadContrato(string rut, string numero, string poliza)
        {

            Contrato contrato = new Contrato();
            List<Contrato> listContrato = new List<Contrato>();
            listContrato = contrato.ListarContratoDetalleOpcion(rut, numero, poliza);

            if (listContrato.Count() == 0)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                GridContratos.ItemsSource = null;
                return;
            }
            /* foreach (Contrato c in listContrato)
             {
                 //VentanaMensaje.ventana("entro __");
                 //dataGridDatos.Rows.Add(c.Numero, c.RutCliente, c.Nombre, c.Apellido, c.Plan, c.Poliza);
             }*/
            GridContratos.ItemsSource = listContrato;
        }
        private void btnBuscarCri_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                if ((this.comboPoliza.SelectedIndex == -1 || this.comboPoliza.SelectedIndex == 0) && this.texCriDigito.Text.Trim() == "" && this.texCriRut.Text.Trim() == "" && txtNumero.Text.Trim() == "")
                {
                    LoadAllContrato();
                }
                else
                {
                    string rut = "";
                    if (this.texCriRut.Text.Trim() != "" && this.texCriDigito.Text.Trim() != "")
                    {
                        rut = this.texCriRut.Text.Trim() + "-" + this.texCriDigito.Text.Trim();
                    }
                    string poliza = "";
                    if ((this.comboPoliza.SelectedIndex != -1 && this.comboPoliza.SelectedIndex != 0))
                    {
                        poliza = ((ComboBoxItem)comboPoliza.SelectedItem).Content.ToString() ;
                    }
                    // VentanaMensaje.ventana("rut "+ rut+ "txtNumero " + this.txtNumero.Text.Trim() + "comboPoliza " + poliza);
                    LoadContrato(rut, this.txtNumero.Text.Trim(), poliza);
                }
            }
            catch
            {
                VentanaMensaje.ventana("No ha podido Buscar");
            }
        }

      
        private void btnVerContrato_Click_2(object sender, RoutedEventArgs e)
        {
            Contrato contrato = new Contrato();
            List<Contrato> listContrato = new List<Contrato>();
            contrato.Numero = (GridContratos.SelectedItem as Contrato).Numero;
            listContrato = contrato.ListarContratoId(contrato);

            contrato.RutCliente = listContrato[0].RutCliente;
            contrato.Nombre = listContrato[0].Nombre;
            contrato.Apellido = listContrato[0].Apellido;
            contrato.FechaNacimiento = listContrato[0].FechaNacimiento;
            contrato.Numero = listContrato[0].Numero;
            contrato.Vigencia = listContrato[0].Vigencia;
            contrato.DeclaracionSalud = listContrato[0].DeclaracionSalud;
            contrato.Observaciones = listContrato[0].Observaciones;
            contrato.PrimaAnual = listContrato[0].PrimaAnual;
            contrato.PrimaMensual = listContrato[0].PrimaMensual;
            contrato.Poliza = listContrato[0].Poliza;
            contrato.FechaInicioVigencia = listContrato[0].FechaInicioVigencia;
            contrato.FechaFinVigencia = listContrato[0].FechaFinVigencia;
            contrato.CodigoPlan = listContrato[0].CodigoPlan;

            contrato.RutCliente = listContrato[0].RutCliente;

             //DetalleContrato.getInstancia().VerContrato(contrato);
            DetalleContrato ver = DetalleContrato.getInstancia();
            ver.VerContrato(contrato);
            ver.Visibility = Visibility.Visible;
            
            //ver.VerContrato(contrato);
            //ver.Show();


        }

        private void btnBorrar_clic(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime thisDay = DateTime.Today;
                Contrato contrato = new Contrato();
                contrato.Vigencia = Convert.ToBoolean(1);
                contrato.Numero = (GridContratos.SelectedItem as Contrato).Numero;

                contrato.FechaFinVigencia = DateTime.Parse(thisDay.ToString("d"));
                if (VentanaMensaje.ventanaConfirm("Desea dejar sin vigencia el contrato N°" + contrato.Numero))
                {
                    if (contrato.Eliminar(contrato))
                    {
                        VentanaMensaje.ventana(" ya no esta vigente el Contrato N°" + contrato.Numero);

                    }
                    else
                    {
                        VentanaMensaje.ventana("Problema");
                    }
                }
            }
            catch
            {
                VentanaMensaje.ventana("Problema con dejar sin vigencia el contrato");
            }
            
               
                   
                
           
        }
    }
}
