﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para BuElMo.xaml
    /// </summary>
    public partial class ListarContrato : MetroWindow
    {
       
        public ListarContrato()
        {
            InitializeComponent();
            //CargaTitulo();
            LoadAllUsers();


        }
        public void CargaTitulo()
        {
            /*
            DataGridTextColumn c1 = new DataGridTextColumn();
            c1.Header = "Rut";           
            GridClientes.Columns.Add(c1);
            c1.IsReadOnly = true;
            c1 = null;

            DataGridTextColumn c2 = new DataGridTextColumn();
            c2.Header = "Nombre";
            GridClientes.Columns.Add(c2);
            c2.IsReadOnly = true;
            c2 = null;

            DataGridTextColumn c3 = new DataGridTextColumn();
            c3.Header = "Apellido";
            GridClientes.Columns.Add(c3);
            c3.IsReadOnly = true;
            c3 = null;

            DataGridTextColumn c4 = new DataGridTextColumn();
            c4.Header = "FechaNacimiento";
            GridClientes.Columns.Add(c4);
            c4.IsReadOnly = true;
            c4 = null;

            DataGridTextColumn c5 = new DataGridTextColumn();
            c5.Header = "Sexo";
            GridClientes.Columns.Add(c5);
            c5.IsReadOnly = true;
            c5 = null;

            DataGridTextColumn c6 = new DataGridTextColumn();
            c6.Header = "EstadoCivil";
            GridClientes.Columns.Add(c6);
            c6.IsReadOnly = true;
            c6 = null;

            DataGridTemplateColumn t = new DataGridTemplateColumn();
            */
           /* Cliente cli = new Cliente();
            cli.Nombres = "Andres";
            cli.Apellidos = "Silva";
            cli.RutCliente = "1687811-2";
            List < Cliente > listCliente= new List<Cliente>();
            listCliente.Add(cli);
            GridClientes.ItemsSource = listCliente;
            */
        }

        public void LoadAllUsers()
        {

            Cliente cliente = new Cliente();
            List<Cliente> listCliente = new List<Cliente>();
          
            listCliente = cliente.ListarClienteDetalle();
            if (listCliente.Count() == 0 || listCliente==null)
            {
                VentanaMensaje.ventana("No hay datos para mostrar");
                return;
            }
           


          
            GridClientes.ItemsSource = listCliente;



        }


        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            MainWindow ver = new MainWindow();
            ver.Show();
            this.Close();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btn_borrar_Click(object sender, RoutedEventArgs e)
        {
            Contrato c = new Contrato();
            Cliente cli = new Cliente();
            cli.RutCliente = (GridClientes.SelectedItem as Cliente).RutCliente;

            //var Id_Producto = (GridClientes.SelectedItem as Cliente).RutCliente;

            if (VentanaMensaje.ventanaConfirm("Esta seguro que desea eliminar al Cliente " + cli.RutCliente + "? "))
            {



                List<Cliente> listCliente = new List<Cliente>();
                listCliente = c.BuscarCliente(cli);
                if (listCliente != null)
                {
                    if (listCliente.Count > 0)
                    {
                        VentanaMensaje.ventana("No se puede eliminar un Cliente que tiene plan");
                    }
                    else
                    {
                        if (cli.DeleteCliente(cli))
                        {
                            VentanaMensaje.ventana("Cliente fue eliminado con exito");
                            LoadAllUsers();
                            //this.limpiar();
                        }
                        else
                        {
                            VentanaMensaje.ventana("Hay un problema eliminando el usuario!!, vuelva a intentarlo");
                        }
                    }
                }
                else
                {
                    VentanaMensaje.ventana("Hay un problema eliminando el usuario!, vuelva a intentarlo");
                }

            }
        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            AgregarCliente AgregarCliente = new AgregarCliente();
            AgregarCliente.Show();
            this.Close();
        }
    }
}
