﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para VerCliente.xaml
    /// </summary>
    public partial class VerCliente : MetroWindow
    {
        public VerCliente()
        {
            InitializeComponent();
        }

        public void CargarDatos(Cliente cliente)
        {
            txtRut.Text = cliente.RutCliente;
            txtNombre.Text = cliente.Nombres;
            txtApellidos.Text = cliente.Apellidos;
            txtSexo.Text = cliente.Sexo;
            txtECivil.Text = cliente.EstadoCivil;
            txtFNacimiento.Text = Convert.ToDateTime(cliente.FechaNacimiento).ToString("dd/MM/yyyy");
        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
