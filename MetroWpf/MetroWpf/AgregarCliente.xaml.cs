﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Behaviours;
using CapaNegocio;
using System.Text.RegularExpressions;

namespace MetroWpf
{
    /// <summary>
    /// Lógica de interacción para AgregarCliente.xaml
    /// </summary>
    public partial class AgregarCliente : MetroWindow
    {
        DateTime thisDay = DateTime.Today;
        public AgregarCliente()
        {
            InitializeComponent();
        }
        public void limpiar()
        {
            this.TextRut.Text = "";
            this.TextDigito.Text = "";
            this.TextNombre.Text = "";
            this.TextApellido.Text = "";
           
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnagreCli_Click(object sender, RoutedEventArgs e)
        {
            Cliente cliente = new Cliente();
            Validar valida = new Validar();
            //VentanaMensaje.ventana(thisDay.ToString("d"));
            //VentanaMensaje.ventana(DatePickerFechaNac.DisplayDate.ToShortDateString());
            string str = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(DatePickerFechaNac.DisplayDate.ToShortDateString()));
            int edad = Convert.ToInt32(str);
            if (edad < 18)
            {
                VentanaMensaje.ventana("El Cliente debe ser mayor de edad", "Advertencia!");
                return;
            }
            if (this.TextRut.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el rut", "Advertencia!");
                return;
            }

            if (this.TextDigito.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Digito Verificador", "Advertencia!");
                return;
            }

            if (this.TextNombre.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Nombre del Cliente", "Advertencia!");
                return;
            }

            if (this.TextApellido.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Apellido del Cliente", "Advertencia!");
                return;
            }

            if (this.DatePickerFechaNac.DisplayDate.ToShortDateString() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar Fecha de Nacimiento", "Advertencia!");
                return;
            }

            if (this.comboSexo.SelectedIndex == 0 || this.comboSexo.SelectedIndex == -1)
            {
                VentanaMensaje.ventana("Debe Seleccionar el Sexo", "Advertencia!");
                return;
            }
            if (this.comboEstadoCivil.SelectedIndex == 0 || this.comboEstadoCivil.SelectedIndex == -1)
            {
                VentanaMensaje.ventana("Debe Seleccionar el Estado Civil", "Advertencia!");
                return;
            }

            cliente.RutCliente = this.TextRut.Text.Trim() + "-" + this.TextDigito.Text.Trim().ToUpper();
            cliente.Nombres = this.TextNombre.Text.Trim().ToUpper();
            cliente.Apellidos = this.TextApellido.Text.Trim().ToUpper();
            cliente.FechaNacimiento = DateTime.Parse(DatePickerFechaNac.DisplayDate.ToShortDateString());
            cliente.IdEstadoCivil = this.comboEstadoCivil.SelectedIndex;
            cliente.IdSexo = this.comboSexo.SelectedIndex;
            List<Cliente> cli = new List<Cliente>();
            cli = cliente.ListarClienteId(cliente);
            if (cli != null)
            {
                if (cli.Count == 0)
                {
                    if (cliente.Agregar(cliente))
                    {
                        VentanaMensaje.ventana("Cliente ingresado correctamente ", "Advertencia!");
                        this.limpiar();
                    }
                    else
                    {
                        VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momento !! c", "Advertencia!");
                    }
                }
                else if (cli.Count > 0)
                {
                    VentanaMensaje.ventana("El usuario ya existe, ingrese otro usuario !! b", "Advertencia!");

                }

            }
            else
            {
                VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momemnto!! ", "Advertencia!");
            }


        }

        private void btnagreLimpiar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnagreVolver_Click(object sender, RoutedEventArgs e)
        {
            BuElMo BuElMo = new BuElMo();
            BuElMo.Show();
            this.Close();
        }

        public void EditarCliente(Cliente cliente)
        {
            this.btnagreCli.Visibility = Visibility.Hidden;
            this.btnEditarCli.Visibility = Visibility.Visible;
            string input = cliente.RutCliente;
            string pattern = "-";            // Split on hyphens

            string[] substrings = Regex.Split(input, pattern);
            this.TextRut.Text = substrings[0];
            this.TextDigito.Text = substrings[1];
            this.TextApellido.Text = cliente.Apellidos;
            this.TextNombre.Text = cliente.Nombres;
            this.comboSexo.SelectedIndex = cliente.IdSexo;
            this.comboEstadoCivil.SelectedIndex = cliente.IdEstadoCivil;
            this.txtRutOriginal.Text = cliente.RutCliente;

        }
        public void VerCliente(Cliente cliente)
        {

        }

        private void btnEditarCli_Click(object sender, RoutedEventArgs e)
        {
            Cliente cliente = new Cliente();
            Validar valida = new Validar();
            //VentanaMensaje.ventana(thisDay.ToString("d"));
            //VentanaMensaje.ventana(DatePickerFechaNac.DisplayDate.ToShortDateString());
            string str = valida.DiferenciaFechas_anio(DateTime.Parse(thisDay.ToString("d")), DateTime.Parse(DatePickerFechaNac.DisplayDate.ToShortDateString()));
            int edad = Convert.ToInt32(str);
            if (edad < 18)
            {
                VentanaMensaje.ventana("El Cliente debe ser mayor de edad", "Advertencia!");
                return;
            }
            if (this.TextRut.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el rut", "Advertencia!");
                return;
            }

            if (this.TextDigito.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Digito Verificador", "Advertencia!");
                return;
            }

            if (this.TextNombre.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Nombre del Cliente", "Advertencia!");
                return;
            }

            if (this.TextApellido.Text.Trim() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar el Apellido del Cliente", "Advertencia!");
                return;
            }

            if (this.DatePickerFechaNac.DisplayDate.ToShortDateString() == "")
            {
                VentanaMensaje.ventana("Debe Ingresar Fecha de Nacimiento", "Advertencia!");
                return;
            }

            if (this.comboSexo.SelectedIndex == 0 || this.comboSexo.SelectedIndex == -1)
            {
                VentanaMensaje.ventana("Debe Seleccionar el Sexo", "Advertencia!");
                return;
            }
            if (this.comboEstadoCivil.SelectedIndex == 0 || this.comboEstadoCivil.SelectedIndex == -1)
            {
                VentanaMensaje.ventana("Debe Seleccionar el Estado Civil", "Advertencia!");
                return;
            }

            cliente.RutCliente = this.TextRut.Text.Trim() + "-" + this.TextDigito.Text.Trim().ToUpper();
            cliente.Nombres = this.TextNombre.Text.Trim().ToUpper();
            cliente.Apellidos = this.TextApellido.Text.Trim().ToUpper();
            cliente.FechaNacimiento = DateTime.Parse(DatePickerFechaNac.DisplayDate.ToShortDateString());
            cliente.IdEstadoCivil = this.comboEstadoCivil.SelectedIndex;
            cliente.IdSexo = this.comboSexo.SelectedIndex;
           
                    if (cliente.ActualizarClientebd(cliente, txtRutOriginal.Text))
                    {
                        VentanaMensaje.ventana("Cliente ingresado correctamente ", "Advertencia!");
                        this.limpiar();
                    }
                    else
                    {
                        VentanaMensaje.ventana("Problema ingresando los datos. Vuelva a intentarlo en un momento !! c", "Advertencia!");
                    }
                

          

        }


    }
}
